# -*- coding: utf-8 mode: shell-script -*-

Test       : Localized basis from LDA states and ACBN0 functional
Program    : octopus
TestGroups : long-run, periodic_systems, lda_u
Enabled    : Yes

ExtraFile: 09-basis_from_states.fhi

Input      : 09-basis_from_states.01-lda.inp
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1

match ; Total k-points   ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 1

Precision: 1.96e-06
match ;     Total energy           ; GREPFIELD(static/info, 'Total       =', 3) ; -39.2848026
Precision: 1.12e-07
match ;     Ion-ion energy         ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -22.49518607
Precision: 3.29e-07
match ;     Eigenvalues sum        ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -6.585820030000001
Precision: 6.06e-07
match ;     Hartree energy         ; GREPFIELD(static/info, 'Hartree     =', 3) ; 12.1292365
Precision: 2.78e-07
match ;     Exchange energy        ; GREPFIELD(static/info, 'Exchange    =', 3) ; -5.56819289
Precision: 2.83e-07
match ;     Correlation energy     ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.56648484
Precision: 1.83e-08
match ;     Kinetic energy         ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 36.63318005
Precision: 2.97e-09
match ;     External energy        ; GREPFIELD(static/info, 'External    =', 3) ; -59.41735553

Precision: 1.00e-04
match ;     k-point 1 (x)     ; GREPFIELD(static/info, '#k =       1', 7) ; 0.0
match ;     k-point 1 (y)     ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
match ;     k-point 1 (z)     ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 5.54e-06
match ;     Eigenvalue  1     ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -1.107054
Precision: 5.54e-06
match ;     Eigenvalue  2     ; GREPFIELD(static/info, '#k =       1', 3, 2) ; -1.107054
Precision: 1.35e-05
match ;     Eigenvalue  4     ; GREPFIELD(static/info, '#k =       1', 3, 4) ; -0.270262
Precision: 2.99e-14
match ;     Eigenvalue  5     ; GREPFIELD(static/info, '#k =       1', 3, 5) ; 0.597026

Input      : 09-basis_from_states.02-acbn0.inp
Precision: 8e-5
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1
match ; Total k-points   ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 1

Precision: 1.96e-07
match ;    Total energy          ; GREPFIELD(static/info, 'Total       =', 3) ; -39.28372234
Precision: 1.12e-07
match ;    Ion-ion energy        ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -22.49518607
Precision: 4.43e-07
match ;    Eigenvalues sum       ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -8.85000376
Precision: 6.07e-08
match ;    Hartree energy        ; GREPFIELD(static/info, 'Hartree     =', 3) ; 12.13210658
Precision: 5.57e-14
match ;    Exchange energy       ; GREPFIELD(static/info, 'Exchange    =', 3) ; -5.56871656
Precision: 2.83e-07
match ;    Correlation energy    ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.56650376
Precision: 3.66e-13
match ;    Kinetic energy        ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 36.63784564
Precision: 2.97e-07
match ;    External energy       ; GREPFIELD(static/info, 'External    =', 3) ; -59.42431791
Precision: 5.25e-09
match ;    Hubbard energy        ; GREPFIELD(static/info, 'Hubbard     =', 3) ; 0.00104998

Precision: 3.78e-05
match ;    U states     ; LINEFIELD(static/effectiveU, 3, 3) ; 0.756686
Precision: 2.00e-14
match ;    Occupation Ni2 up-down 3d4    ; LINEFIELD(static/occ_matrices, -2, 2) ; 1.9990744
Precision: 1.00e-07
match ;    Occupation Ni2 up-down 3d5    ; LINEFIELD(static/occ_matrices, -1, 3) ; 1.99907419

Precision: 1.00e-04
match ;    k-point 1 (x)    ; GREPFIELD(static/info, '#k =       1', 7) ; 0.0
match ;    k-point 1 (y)    ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
match ;    k-point 1 (z)    ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 7.42e-08
match ;    Eigenvalue  1    ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -1.4844270000000002
Precision: 7.42e-06
match ;    Eigenvalue  2    ; GREPFIELD(static/info, '#k =       1', 3, 2) ; -1.484426
Precision: 1.35e-05
match ;    Eigenvalue  4    ; GREPFIELD(static/info, '#k =       1', 3, 4) ; -0.270246
Precision: 2.99e-05
match ;    Eigenvalue  5    ; GREPFIELD(static/info, '#k =       1', 3, 5) ; 0.597045

Input: 09-basis_from_states.03-intersite.inp
Precision: 8e-5
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1
match ; Total k-points   ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 1

Precision: 5.50e-09
match ;    Total energy          ; GREPFIELD(static/info, 'Total       =', 3) ; -40.532592035
Precision: 1.12e-07
match ;    Ion-ion energy        ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -22.49518607
Precision: 5.67e-08
match ;    Eigenvalues sum       ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -11.34605152
Precision: 6.07e-08
match ;    Hartree energy        ; GREPFIELD(static/info, 'Hartree     =', 3) ; 12.13415992
Precision: 2.78e-07
match ;    Exchange energy       ; GREPFIELD(static/info, 'Exchange    =', 3) ; -5.56908682
Precision: 2.83e-07
match ;    Correlation energy    ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.56651725
Precision: 1.83e-07
match ;    Kinetic energy        ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 36.64119433
Precision: 2.97e-07
match ;    External energy       ; GREPFIELD(static/info, 'External    =', 3) ; -59.42925641
Precision: 6.24e-09
match ;    Hubbard energy        ; GREPFIELD(static/info, 'Hubbard     =', 3) ; -1.247899815

Precision: 3.78e-05
match ;    U states     ; LINEFIELD(static/effectiveU, 3, 3) ; 0.756721
Precision: 3.50e-16
match ;    V states     ; GREPFIELD(static/info, 'Effective intersite V', 5, 3) ; 0.035013
Precision: 1.73e-05
match ;    V states     ; GREPFIELD(static/info, 'Effective intersite V', 5, 4) ; 0.034533
Precision: 3.50e-16
match ;    V states     ; GREPFIELD(static/info, 'Effective intersite V', 5, 5) ; 0.035034

Precision: 1.00e-07
match ;    Occupation Ni2 up-down 3d4    ; LINEFIELD(static/occ_matrices, -2, 2) ; 1.9991205600000002
Precision: 2.00e-14
match ;    Occupation Ni2 up-down 3d5    ; LINEFIELD(static/occ_matrices, -1, 3) ; 1.99912029

Precision: 1.00e-04
match ;    k-point 1 (x)    ; GREPFIELD(static/info, '#k =       1', 7) ; 0.0
match ;    k-point 1 (y)    ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
match ;    k-point 1 (z)    ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 1.90e-14
match ;    Eigenvalue  1    ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -1.9004409999999998
Precision: 1.90e-14
match ;    Eigenvalue  2    ; GREPFIELD(static/info, '#k =       1', 3, 2) ; -1.9004409999999998
Precision: 1.35e-05
match ;    Eigenvalue  4    ; GREPFIELD(static/info, '#k =       1', 3, 4) ; -0.270225
Precision: 2.99e-05
match ;    Eigenvalue  5    ; GREPFIELD(static/info, '#k =       1', 3, 5) ; 0.597069