foreach (test IN ITEMS
		01-octopus_basics-getting_started
		02-octopus_basics-basic_input_options
		03-octopus_basics-total_energy_convergence
		04-octopus_basics-visualization
		05-octopus_basics-centering_a_geometry
		06-octopus_basics-periodic_systems
		07-octopus_basics-time_dependent_propagation
)
	Octopus_add_test(${test}
			TEST_NAME ${curr_path}/${test}
	)
endforeach ()
