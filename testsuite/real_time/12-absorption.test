# -*- coding: utf-8 mode: shell-script -*-

Test       : Absorption spectrum
Program    : octopus
TestGroups : short-run, real_time
Enabled    : Yes

# Note: the results differ from 13-absorption-spin.test because here we are doing
# an odd number of electrons without spin-polarization, which is wrong.

# ground state
Processors : 2
Input      : 12-absorption.01-gs.inp
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1
Precision: 2.91e-07
match ;  Initial energy  ; GREPFIELD(static/info, 'Total       =', 3) ; -5.81013691


if(available mpi); then
Precision: 2.20e+01
  match ; IO Profiling files open ;  GREPFIELD(out, 'Number of file open', 6) ; 44.0
Precision: 2.20e+01
  match ; IO Profiling files close ; GREPFIELD(out, 'Number of file close', 6) ; 44.0
else
Precision: 2.20e+01
  match ; IO Profiling files open ;  GREPFIELD(out, 'Number of file open', 6) ; 44.0
Precision: 2.20e+01
  match ; IO Profiling files close ; GREPFIELD(out, 'Number of file close', 6) ; 44.0
endif


Processors : 4
Input      : 12-absorption.02-td.inp
Precision: 8.30e-14
match ;  Energy [step   1]  ; LINEFIELD(td.general/energy, -101, 3) ; -5.81013696681837
Precision: 7.03e-14
match ;  Energy [step  25]  ; LINEFIELD(td.general/energy, -76, 3) ; -5.809755963265225
Precision: 8.16e-14
match ;  Energy [step  50]  ; LINEFIELD(td.general/energy, -51, 3) ; -5.8097559443355
Precision: 7.72e-14
match ;  Energy [step  75]  ; LINEFIELD(td.general/energy, -26, 3) ; -5.8097559297081105
Precision: 1.14e-13
match ;  Energy [step 100]  ; LINEFIELD(td.general/energy, -1, 3) ; -5.809755909085682

Processors : 4
Input      : 12-absorption.03-td-restart.inp
Precision: 1.14e-13
match ;  Energy [step 100]  ; LINEFIELD(td.general/energy, -101, 3) ; -5.809755909085682
Precision: 2.90e-13
match ;  Energy [step 125]  ; LINEFIELD(td.general/energy, -76, 3) ; -5.809755894038739
Precision: 7.43e-14
match ;  Energy [step 150]  ; LINEFIELD(td.general/energy, -51, 3) ; -5.809755872768485
Precision: 2.90e-13
match ;  Energy [step 175]  ; LINEFIELD(td.general/energy, -26, 3) ; -5.80975585964577
Precision: 7.96e-14
match ;  Energy [step 200]  ; LINEFIELD(td.general/energy, -1, 3) ; -5.8097558376989795

Util       : oct-propagation_spectrum
Input      : 12-absorption.04-spectrum.inp

# Issue #1153. This tolerance is too loose, but has been temporarily set to avoid this failing blocking the CI
Precision: 0.003
match ;  Electronic sum rule  ; GREPFIELD(cross_section_vector.1, 'Electronic sum rule', 6) ; 0.963705
Precision: 8.05e-06
match ;  Static polarizability  ; GREPFIELD(cross_section_vector.1, 'Static polarizability (from sum rule)', 8) ; 16.103486
Precision: 1.00e-01
match ;  Energy      1  ; LINEFIELD(cross_section_tensor, -91, 1) ; 1.0
Precision: 2.96e-09
match ;  Sigma       1  ; LINEFIELD(cross_section_tensor, -91, 2) ; 0.059239751
Precision: 6.97e-16
match ;  Anisotropy  1  ; LINEFIELD(cross_section_tensor, -91, 3) ; 0.069679028
Precision: 1.00e-01
match ;  Energy      2  ; LINEFIELD(cross_section_tensor, -81, 1) ; 2.0
Precision: 1.02e-06
match ;  Sigma       2  ; LINEFIELD(cross_section_tensor, -81, 2) ; 0.2037328
Precision: 1.13e-07
match ;  Anisotropy  2  ; LINEFIELD(cross_section_tensor, -81, 3) ; 0.22600682
Precision: 1.00e-01
match ;  Energy      3  ; LINEFIELD(cross_section_tensor, -71, 1) ; 3.0
Precision: 1.72e-07
match ;  Sigma       3  ; LINEFIELD(cross_section_tensor, -71, 2) ; 0.34337428
Precision: 1.76e-06
match ;  Anisotropy  3  ; LINEFIELD(cross_section_tensor, -71, 3) ; 0.3526466
Precision: 1.00e-01
match ;  Energy      4  ; LINEFIELD(cross_section_tensor, -61, 1) ; 4.0
Precision: 1.89e-06
match ;  Sigma       4  ; LINEFIELD(cross_section_tensor, -61, 2) ; 0.3783206
Precision: 1.80e-06
match ;  Anisotropy  4  ; LINEFIELD(cross_section_tensor, -61, 3) ; 0.3594375
Precision: 1.00e-01
match ;  Energy      5  ; LINEFIELD(cross_section_tensor, -51, 1) ; 5.0
Precision: 1.40e-07
match ;  Sigma       5  ; LINEFIELD(cross_section_tensor, -51, 2) ; 0.27915326
Precision: 1.24e-07
match ;  Anisotropy  5  ; LINEFIELD(cross_section_tensor, -51, 3) ; 0.24849213
Precision: 1.00e-01
match ;  Energy      6  ; LINEFIELD(cross_section_tensor, -41, 1) ; 6.0
Precision: 6.04e-08
match ;  Sigma       6  ; LINEFIELD(cross_section_tensor, -41, 2) ; 0.12083074
Precision: 5.24e-08
match ;  Anisotropy  6  ; LINEFIELD(cross_section_tensor, -41, 3) ; 0.10488981
Precision: 1.00e-01
match ;  Energy      7  ; LINEFIELD(cross_section_tensor, -31, 1) ; 7.0
Precision: 6.09e-09
match ;  Sigma       7  ; LINEFIELD(cross_section_tensor, -31, 2) ; 0.012185528000000001
Precision: 1.85e-16
match ;  Anisotropy  7  ; LINEFIELD(cross_section_tensor, -31, 3) ; 0.018453296
Precision: 1.00e-01
match ;  Energy      8  ; LINEFIELD(cross_section_tensor, -21, 1) ; 8.0
Precision: 4.15e-09
match ;  Sigma       8  ; LINEFIELD(cross_section_tensor, -21, 2) ; -0.0082943152
Precision: 4.57e-09
match ;  Anisotropy  8  ; LINEFIELD(cross_section_tensor, -21, 3) ; 0.0091412594
Precision: 1.00e-01
match ;  Energy      9  ; LINEFIELD(cross_section_tensor, -11, 1) ; 9.0
Precision: 5.89e-14
match ;  Sigma       9  ; LINEFIELD(cross_section_tensor, -11, 2) ; 0.011782469
Precision: 9.44e-09
match ;  Anisotropy  9  ; LINEFIELD(cross_section_tensor, -11, 3) ; 0.018882941
Precision: 1.00e-01
match ;  Energy     10  ; LINEFIELD(cross_section_tensor, -1, 1) ; 10.0
Precision: 1.70e-16
match ;  Sigma      10  ; LINEFIELD(cross_section_tensor, -1, 2) ; 0.017048685
Precision: 8.51e-09
match ;  Anisotropy 10  ; LINEFIELD(cross_section_tensor, -1, 3) ; 0.017015165

Util       : oct-propagation_spectrum
Input      : 12-absorption.05-spectrum_compressed_sensing.inp
Precision: 5.0e-03
match ;  Electronic sum rule  ; GREPFIELD(cross_section_vector.1, 'Electronic sum rule', 6) ; 0.99682
Precision: 6.00e-02
match ;  Static polarizability  ; GREPFIELD(cross_section_vector.1, 'Static polarizability (from sum rule)', 8) ; 12.206788

#match ; Energy      1 ; LINE(cross_section_tensor, 230, 1)  ; 1.0
#match ; Sigma       1 ; LINE(cross_section_tensor, 23, 21) ;
#match ; Anisotropy  1 ; LINE(cross_section_tensor, 23, 41) ;

#match ; Energy      2 ; LINE(cross_section_tensor, 39, 1)  ; 0.27000000E+01
#match ; Sigma       2 ; LINE(cross_section_tensor, 39, 21) ; 0.24611830E+01
#match ; Anisotropy  2 ; LINE(cross_section_tensor, 39, 41) ; 0.47660604E+01

#match ; Energy      3 ; LINE(cross_section_tensor, 347, 1)  ; 0.33400000E+01
#match ; Sigma       3 ; LINE(cross_section_tensor, 347, 21) ; 0.33193123E+02
#match ; Anisotropy  3 ; LINE(cross_section_tensor, 347, 41) ; 0.40653000E+02

#match ; Energy      4 ; LINE(cross_section_tensor, 82, 1)  ; 0.70000000E+01
#match ; Sigma       4 ; LINE(cross_section_tensor, 82, 21) ; 0.10806835E+00
#match ; Anisotropy  4 ; LINE(cross_section_tensor, 82, 41) ; 0.12072535E+00

#match ; Energy      5 ; LINE(cross_section_tensor, 118, 1) ; 0.10600000E+02
#match ; Sigma       5 ; LINE(cross_section_tensor, 118, 21); 0.52145360E-01
#match ; Anisotropy  5 ; LINE(cross_section_tensor, 118, 41); 0.10097905E+00

#match ; Energy      6 ; LINE(cross_section_tensor, 163, 1) ; 0.15100000E+02
#match ; Sigma       6 ; LINE(cross_section_tensor, 163, 21); 0.42107780E-01
#match ; Anisotropy  6 ; LINE(cross_section_tensor, 163, 41); 0.81541365E-01


Util       : oct-propagation_spectrum
Input      : 12-absorption.06-power_spectrum.inp
Precision: 0.07
match ; Energy  0 x ; LINEFIELD(dipole_power, 14, 2) ; 0.66225480E-01
match ; Energy  0 y ; LINEFIELD(dipole_power, 14, 3) ; 0.35939672E-01
match ; Energy  0 z ; LINEFIELD(dipole_power, 14, 4) ; 0.12022952E-28

match ; Energy  1 x ; LINEFIELD(dipole_power, 24, 2) ; 0.62685953E-01
match ; Energy  1 y ; LINEFIELD(dipole_power, 24, 3) ; 0.35928656E-01
match ; Energy  1 z ; LINEFIELD(dipole_power, 24, 4) ; 0.11241176E-28

match ; Energy 10 x ; LINEFIELD(dipole_power, 114, 2) ; 0.31239081E-03
match ; Energy 10 y ; LINEFIELD(dipole_power, 114, 3) ; 0.31644760E-03
match ; Energy 10 z ; LINEFIELD(dipole_power, 114, 4) ; 0.61450976E-31

Util       : oct-propagation_spectrum
Input      : 12-absorption.07-spectrum_cosine.inp

Precision: 1.00e-01
match ;  Energy      1  ; LINEFIELD(cross_section_tensor, -91, 1) ; 1.0
Precision: 6.15e-08
match ;  Sigma       1  ; LINEFIELD(cross_section_tensor, -91, 2) ; 0.12308631
Precision: 6.50e-02
match ;  Anisotropy  1  ; LINEFIELD(cross_section_tensor, -91, 3) ; 0.13
Precision: 1.00e-01
match ;  Energy      2  ; LINEFIELD(cross_section_tensor, -81, 1) ; 2.0
Precision: 7.99e-08
match ;  Sigma       2  ; LINEFIELD(cross_section_tensor, -81, 2) ; 0.15977104
Precision: 7.61e-08
match ;  Anisotropy  2  ; LINEFIELD(cross_section_tensor, -81, 3) ; 0.15218328
Precision: 1.00e-01
match ;  Energy      3  ; LINEFIELD(cross_section_tensor, -71, 1) ; 3.0
Precision: 3.41e-08
match ;  Sigma       3  ; LINEFIELD(cross_section_tensor, -71, 2) ; 0.068215839
Precision: 3.25e-15
match ;  Anisotropy  3  ; LINEFIELD(cross_section_tensor, -71, 3) ; 0.06504665
Precision: 1.00e-01
match ;  Energy      4  ; LINEFIELD(cross_section_tensor, -61, 1) ; 4.0
Precision: 5.43e-08
match ;  Sigma       4  ; LINEFIELD(cross_section_tensor, -61, 2) ; -0.10863633
Precision: 7.70e-04
match ;  Anisotropy  4  ; LINEFIELD(cross_section_tensor, -61, 3) ; 0.1541
Precision: 1.00e-01
match ;  Energy      5  ; LINEFIELD(cross_section_tensor, -51, 1) ; 5.0
Precision: 2.59e-15
match ;  Sigma       5  ; LINEFIELD(cross_section_tensor, -51, 2) ; -0.25902244
Precision: 1.35e-08
match ;  Anisotropy  5  ; LINEFIELD(cross_section_tensor, -51, 3) ; 0.26950695
Precision: 1.00e-01
match ;  Energy      6  ; LINEFIELD(cross_section_tensor, -41, 1) ; 6.0
Precision: 1.47e-07
match ;  Sigma       6  ; LINEFIELD(cross_section_tensor, -41, 2) ; -0.29386929
Precision: 1.38e-05
match ;  Anisotropy  6  ; LINEFIELD(cross_section_tensor, -41, 3) ; 0.276973
Precision: 1.00e-01
match ;  Energy      7  ; LINEFIELD(cross_section_tensor, -31, 1) ; 7.0
Precision: 1.13e-07
match ;  Sigma       7  ; LINEFIELD(cross_section_tensor, -31, 2) ; -0.22681891
Precision: 1.02e-06
match ;  Anisotropy  7  ; LINEFIELD(cross_section_tensor, -31, 3) ; 0.2046105
Precision: 1.00e-01
match ;  Energy      8  ; LINEFIELD(cross_section_tensor, -21, 1) ; 8.0
Precision: 7.28e-08
match ;  Sigma       8  ; LINEFIELD(cross_section_tensor, -21, 2) ; -0.14567168
Precision: 6.57e-08
match ;  Anisotropy  8  ; LINEFIELD(cross_section_tensor, -21, 3) ; 0.13138684
Precision: 1.00e-01
match ;  Energy      9  ; LINEFIELD(cross_section_tensor, -11, 1) ; 9.0
Precision: 5.56e-08
match ;  Sigma       9  ; LINEFIELD(cross_section_tensor, -11, 2) ; -0.11117993
Precision: 5.26e-08
match ;  Anisotropy  9  ; LINEFIELD(cross_section_tensor, -11, 3) ; 0.10525676
Precision: 1.00e-01
match ;  Energy     10  ; LINEFIELD(cross_section_tensor, -1, 1) ; 10.0
Precision: 5.50e-08
match ;  Sigma      10  ; LINEFIELD(cross_section_tensor, -1, 2) ; -0.10994377
Precision: 5.30e-07
match ;  Anisotropy 10  ; LINEFIELD(cross_section_tensor, -1, 3) ; 0.1060796

Util       : oct-propagation_spectrum
Input      : 12-absorption.08-spectrum_exp.inp

Precision: 1.00e-01
match ;  Energy      1  ; LINEFIELD(cross_section_tensor, -91, 1) ; 1.0
Precision: 4.52e-08
match ;  Sigma       1  ; LINEFIELD(cross_section_tensor, -91, 2) ; 0.090342979
Precision: 4.70e-08
match ;  Anisotropy  1  ; LINEFIELD(cross_section_tensor, -91, 3) ; 0.09406863700000001
Precision: 1.00e-01
match ;  Energy      2  ; LINEFIELD(cross_section_tensor, -81, 1) ; 2.0
Precision: 6.10e-08
match ;  Sigma       2  ; LINEFIELD(cross_section_tensor, -81, 2) ; 0.12202846
Precision: 6.20e-08
match ;  Anisotropy  2  ; LINEFIELD(cross_section_tensor, -81, 3) ; 0.12408541
Precision: 1.00e-01
match ;  Energy      3  ; LINEFIELD(cross_section_tensor, -71, 1) ; 3.0
Precision: 6.42e-07
match ;  Sigma       3  ; LINEFIELD(cross_section_tensor, -71, 2) ; 0.1283787
Precision: 6.43e-10
match ;  Anisotropy  3  ; LINEFIELD(cross_section_tensor, -71, 3) ; 0.12853163
Precision: 1.00e-01
match ;  Energy      4  ; LINEFIELD(cross_section_tensor, -61, 1) ; 4.0
Precision: 6.22e-08
match ;  Sigma       4  ; LINEFIELD(cross_section_tensor, -61, 2) ; 0.12437943
Precision: 6.16e-07
match ;  Anisotropy  4  ; LINEFIELD(cross_section_tensor, -61, 3) ; 0.1232414
Precision: 1.00e-01
match ;  Energy      5  ; LINEFIELD(cross_section_tensor, -51, 1) ; 5.0
Precision: 5.83e-07
match ;  Sigma       5  ; LINEFIELD(cross_section_tensor, -51, 2) ; 0.1166235
Precision: 5.74e-08
match ;  Anisotropy  5  ; LINEFIELD(cross_section_tensor, -51, 3) ; 0.11474335
Precision: 1.00e-01
match ;  Energy      6  ; LINEFIELD(cross_section_tensor, -41, 1) ; 6.0
Precision: 5.40e-07
match ;  Sigma       6  ; LINEFIELD(cross_section_tensor, -41, 2) ; 0.1079479
Precision: 5.28e-08
match ;  Anisotropy  6  ; LINEFIELD(cross_section_tensor, -41, 3) ; 0.10569057
Precision: 1.00e-01
match ;  Energy      7  ; LINEFIELD(cross_section_tensor, -31, 1) ; 7.0
Precision: 4.98e-08
match ;  Sigma       7  ; LINEFIELD(cross_section_tensor, -31, 2) ; 0.099521471
Precision: 4.86e-08
match ;  Anisotropy  7  ; LINEFIELD(cross_section_tensor, -31, 3) ; 0.097107706
Precision: 1.00e-01
match ;  Energy      8  ; LINEFIELD(cross_section_tensor, -21, 1) ; 8.0
Precision: 4.59e-08
match ;  Sigma       8  ; LINEFIELD(cross_section_tensor, -21, 2) ; 0.091777253
Precision: 4.47e-08
match ;  Anisotropy  8  ; LINEFIELD(cross_section_tensor, -21, 3) ; 0.089334216
Precision: 1.00e-01
match ;  Energy      9  ; LINEFIELD(cross_section_tensor, -11, 1) ; 9.0
Precision: 4.24e-08
match ;  Sigma       9  ; LINEFIELD(cross_section_tensor, -11, 2) ; 0.084828527
Precision: 4.12e-07
match ;  Anisotropy  9  ; LINEFIELD(cross_section_tensor, -11, 3) ; 0.08242673
Precision: 1.00e-01
match ;  Energy     10  ; LINEFIELD(cross_section_tensor, -1, 1) ; 10.0
Precision: 3.93e-07
match ;  Sigma      10  ; LINEFIELD(cross_section_tensor, -1, 2) ; 0.07865519
Precision: 3.82e-08
match ;  Anisotropy 10  ; LINEFIELD(cross_section_tensor, -1, 3) ; 0.076331762