# -*- coding: utf-8 mode: shell-script -*-

Test       : Free Maxwell propagation using the exponential Gauss propagators
Program    : octopus
TestGroups : long-run, maxwell
Enabled    : Yes

Input      : 15-expgauss.01-exp_gauss2_taylor.inp
Precision: 1e-13
match ;    Tot. Maxwell energy [step 0]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 6, 3) ; 0.2019900047434819
match ;    Tot. Maxwell energy [step 50]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 56, 3) ; 0.201995511012985
match ;    Tot. Maxwell energy [step 100]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 106, 3) ; 0.2020010190356021

Precision: 1e-14
match ;    Ez at PML border (x=-14,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 35, 3) ; -0.0018518502340010802
match ;    Ez at PML border (x= 14,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 3283, 3) ; -8.5683732201202e-06
match ;    Ez in medium, center (x=  0,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 1659, 3) ; 0.0137858256784623
match ;    Ez in medium, focus (x=0.5,y=-2.5,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 1712, 3) ; -0.00280938180629834
match ;    Ez in vacuum, pw max (x=6,y=9.5,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 2374, 3) ; -0.000788998743021283
match ;    Ez in vacuum, focus (x=5.5,y=0,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 2297, 3) ; 0.000556990698420762

Input      : 15-expgauss.02-exp_gauss2_lanczos.inp
Precision: 1e-13
match ;    Tot. Maxwell energy [step 0]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 6, 3) ; 0.2019900047434819
match ;    Tot. Maxwell energy [step 50]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 56, 3) ; 0.2019900047434206
match ;    Tot. Maxwell energy [step 100]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 106, 3) ; 0.2019900047434254

Precision: 1e-14
match ;    Ez at PML border (x=-14,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 35, 3) ; -0.00185173494716539
match ;    Ez at PML border (x= 14,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 3283, 3) ; -8.568515761549881e-06
match ;    Ez in medium, center (x=  0,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 1659, 3) ; 0.013784018764837501
match ;    Ez in medium, focus (x=0.5,y=-2.5,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 1712, 3) ; -0.0028079976564376104
match ;    Ez in vacuum, pw max (x=6,y=9.5,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 2374, 3) ; -0.000788953600578862
match ;    Ez in vacuum, focus (x=5.5,y=0,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 2297, 3) ; 0.0005572023685456605

Input      : 15-expgauss.03-exp_gauss2_chebyshev.inp
Precision: 1.00e-13
match ;     Tot. Maxwell energy [step 0]     ; LINEFIELD(Maxwell/td.general/maxwell_energy, 6, 3) ; 0.2019900047434819
match ;     Tot. Maxwell energy [step 50]     ; LINEFIELD(Maxwell/td.general/maxwell_energy, 56, 3) ; 0.2019900047434367
match ;     Tot. Maxwell energy [step 100]     ; LINEFIELD(Maxwell/td.general/maxwell_energy, 106, 3) ; 0.2019900047434427

Precision: 1e-14
match ;     Ez at PML border (x=-14,y=  0,z=  0) [step 100]     ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 35, 3) ; -0.0018517349490878951
match ;     Ez at PML border (x= 14,y=  0,z=  0) [step 100]     ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 3283, 3) ; -8.568515777738851e-06
match ;     Ez in medium, center (x=  0,y=  0,z=  0) [step 100]     ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 1659, 3) ; 0.0137840187809154
match ;     Ez in medium, focus (x=0.5,y=-2.5,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 1712, 3) ; -0.0028079976590768304
match ;     Ez in vacuum, pw max (x=6,y=9.5,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 2374, 3) ; -0.000788953606932859
match ;     Ez in vacuum, focus (x=5.5,y=0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 2297, 3) ; 0.000557202371047752


Input      : 15-expgauss.04-exp_gauss1_pml_medium.inp
Precision: 1e-13
match ;    Tot. Maxwell energy [step 0]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 6, 3) ; 0.104507601715533
match ;    Tot. Maxwell energy [step 5]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 11, 3) ; 0.09574076415990536
match ;    Tot. Maxwell energy [step 10]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 16, 3) ; 0.1088300630939463

Precision: 1e-13
match ;    Ez at PML border (x=-14,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.z=0, 35, 3) ; 0.0
match ;    Ez at PML border (x= 14,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.z=0, 3283, 3) ; 0.0
match ;    Ez in medium, center (x=  0,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.z=0, 1659, 3) ; 0.025461379676216003
match ;    Ez in medium, focus (x=0.5,y=-2.5,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.z=0, 1712, 3) ; 0.0427307576315899
match ;    Ez in vacuum, pw max (x=6,y=9.5,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.z=0, 2374, 3) ; 0.05256709665527309
match ;    Ez in vacuum, focus (x=5.5,y=0,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.z=0, 2297, 3) ; -0.00041373686033610005
