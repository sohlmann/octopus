!! Copyright (C) 2023 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

!> This module provides routines for perform the Selected Column of Density Matrix (SCDM) method
!! This follows the original paper by Lin and also implement the SCDM-k version
!! see Journal of Computational Physics 334 (2017) 1-15
module scdm_oct_m
  use debug_oct_m
  use electron_space_oct_m
  use global_oct_m
  use grid_oct_m
  use kpoints_oct_m
  use lalg_basic_oct_m
  use lalg_adv_oct_m
  use mesh_oct_m
  use messages_oct_m
  use namespace_oct_m
  use parser_oct_m
  use profiling_oct_m
  use space_oct_m
  use states_elec_oct_m
  use states_elec_dim_oct_m
  use states_elec_calc_oct_m

  implicit none

  private
  public ::    &
    scdm_t,    &
    scdm_init, &
    dscdm_get_localized_states, &
    zscdm_get_localized_states

  type scdm_t
    private

    real(real64) :: scdm_sigma

  end type scdm_t

contains

  subroutine scdm_init(this, namespace)
    type(scdm_t),         intent(inout) :: this
    type(namespace_t),    intent(in)    :: namespace

    PUSH_SUB(scdm_init)

    ! We first compute the U matrix using the SCDM-k method
    ! and then we compute the corresponding Wannier states
    call parse_variable(global_namespace, 'SCDMsigma', 0.2_real64, this%scdm_sigma)

    POP_SUB(scdm_init)
  end subroutine scdm_init

#include "undef.F90"
#include "real.F90"
#include "scdm_inc.F90"

#include "undef.F90"
#include "complex.F90"
#include "scdm_inc.F90"

end module scdm_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
