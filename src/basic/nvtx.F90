!! Copyright (C) 2019 S. Ohlmann
!!
!! This Source Code Form is subject to the terms of the Mozilla Public
!! License, v. 2.0. If a copy of the MPL was not distributed with this
!! file, You can obtain one at https://mozilla.org/MPL/2.0/.
!!

! This is the fortran part of the wrapper around the NVTX
! (NVIDIA Tools Extension) profiling functions.

#include "global.h"

module nvtx_oct_m

  implicit none

  private
  public ::          &
    nvtx_range_push, &
    nvtx_range_pop

  interface

    subroutine nvtx_range_push(range_name, idx)
      use iso_c_binding
      implicit none

      character(len=*), intent(in)    :: range_name
      integer,          intent(in)    :: idx
    end subroutine nvtx_range_push

    subroutine nvtx_range_pop()
      use iso_c_binding
      implicit none
    end subroutine nvtx_range_pop
  end interface

end module nvtx_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
