#define _GNU_SOURCE

#include <config.h>
#include <fenv.h>
#include <unistd.h>

#ifdef HAVE_VERROU
#include <valgrind/verrou.h>
#endif

void debug_verrou_start_instrumentation() {
#ifdef HAVE_VERROU
    VERROU_START_INSTRUMENTATION;
#endif
}

void debug_verrou_stop_instrumentation() {
#ifdef HAVE_VERROU
    VERROU_STOP_INSTRUMENTATION;
#endif
}

void debug_fenv_start_instrumentation() {
#ifdef __GLIBC__
    feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW | FE_UNDERFLOW);
#endif
}

void debug_fenv_stop_instrumentation() {
#ifdef __GLIBC__
    fedisableexcept(FE_ALL_EXCEPT);
#endif
}
