!! Copyright (C) 2008 X. Andrade, 2024. A Buccheri.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!
module hardware_oct_m

  implicit none
  private
  public ::             &
    cpu_hardware,       &
    cache_t,            &
    hardware_t

  !>@brief Cache
  type cache_t
    integer :: size
    integer :: line_size
  end type cache_t

  interface cache_t
    module procedure cache_t_init
  end interface cache_t

  !>@brief Block-size optimisation for L1 cache size
  type hardware_t
    type(cache_t) :: l1
    type(cache_t) :: l2
    integer :: dblock_size  !< Block size for real(real64) data
    integer :: zblock_size  !< Block size for complex(real64) data
  contains
    procedure :: init => hardware_init
  end type hardware_t

  ! Same as getting sizeof(a)
  !> Number of bytes to store a variable of type real(real64)
  integer, public, parameter :: sizeof_real64 = 8

  !> Number of bytes to store a variable of type complex(real64)
  integer, public, parameter :: sizeof_complex64 = 16

  !> Global instance of CPU hardware specification
  type(hardware_t) :: cpu_hardware

  !> Defaults covers common chip specification for (l1, l2) cache
  type(cache_t), public :: default_l1
  type(cache_t), public :: default_l2

contains

  !> @brief Initialise cache object
  function cache_t_init(data_size, line_size) result(cache)
    type(cache_t) :: cache             !< Cache instance
    integer, intent(in) :: data_size   !< Cache data size in B
    integer, intent(in) :: line_size   !< Line size in B

    cache%size = data_size
    cache%line_size = line_size

  end function cache_t_init


  ! TODO(Alex) Issue 1013. Change hardware class to a free function, and make cache_t instance global.
  !> @brief Initialise hardware object
  !!
  !! The block size is defined such that:
  !!  1. Initially ensure that each block fits into the l1 cache.
  !!  2. The block_size should be a multiple of the cache line:
  !!     block_size - mod( ) ensures any remainder is subtracted, such that (block_size / line_size) returns an integer
  !!  3. Minus 2 lines to avoid powers of 2
  subroutine hardware_init(this, l1_cache, l2_cache)
    class(hardware_t), intent(inout) :: this
    type(cache_t), intent(in) :: l1_cache  !< l1 cache
    type(cache_t), intent(in) :: l2_cache  !< l2 cache

    integer, parameter :: cpu_default_states_per_block = 4  !< Default number of states per block, on CPU

    this%l1 = l1_cache
    this%l2 = l2_cache

    this%dblock_size = this%l1%size / (cpu_default_states_per_block * sizeof_real64)
    this%dblock_size = this%dblock_size - mod(this%dblock_size, this%l1%line_size) - 2*this%l1%line_size

    this%zblock_size = this%l1%size / (cpu_default_states_per_block * sizeof_complex64)
    this%zblock_size = this%zblock_size - mod(this%zblock_size, this%l1%line_size) - 2*this%l1%line_size

  end subroutine hardware_init


end module hardware_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
