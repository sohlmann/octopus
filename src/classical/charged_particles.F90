!! Copyright (C) 2022 M. Oliveira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module charged_particles_oct_m
  use classical_particles_oct_m
  use coulomb_force_oct_m
  use current_to_mxll_field_oct_m
  use debug_oct_m
  use global_oct_m
  use interaction_surrogate_oct_m
  use interaction_enum_oct_m
  use lorentz_force_oct_m
  use messages_oct_m
  use profiling_oct_m
  use quantity_oct_m

  implicit none

  private
  public ::                                           &
    charged_particles_t,                              &
    charged_particles_init,                           &
    charged_particles_copy,                           &
    charged_particles_end,                            &
    charged_particles_init_interaction,               &
    charged_particles_update_quantity,                &
    charged_particles_init_interaction_as_partner,    &
    charged_particles_copy_quantities_to_interaction

  type, extends(classical_particles_t), abstract :: charged_particles_t
    private
    real(real64), allocatable, public :: charge(:)      !< Charge of the particles
  contains
    procedure :: update_quantity => charged_particles_update_quantity
    procedure :: init_interaction_as_partner => charged_particles_init_interaction_as_partner
    procedure :: copy_quantities_to_interaction => charged_particles_copy_quantities_to_interaction
  end type charged_particles_t


contains

  ! ---------------------------------------------------------
  !> The init routine is a module level procedure
  !! This has the advantage that different classes can have different
  !! signatures for the initialization routines because they are not
  !! type-bound and thus also not inherited.
  ! ---------------------------------------------------------
  subroutine charged_particles_init(this, np)
    class(charged_particles_t), intent(inout) :: this
    integer,                    intent(in)    :: np !< Number of particles

    PUSH_SUB(charged_particles_init)

    call classical_particles_init(this, np)
    SAFE_ALLOCATE(this%charge(1:np))
    this%charge = M_ZERO

    call this%quantities%add(quantity_t("charge", updated_on_demand = .false., always_available = .true.))

    this%supported_interactions = [this%supported_interactions, COULOMB_FORCE, LORENTZ_FORCE]
    this%supported_interactions_as_partner = [this%supported_interactions_as_partner, COULOMB_FORCE, CURRENT_TO_MXLL_FIELD]

    POP_SUB(charged_particles_init)
  end subroutine charged_particles_init

  ! ---------------------------------------------------------
  subroutine charged_particles_copy(this, cp_in)
    class(charged_particles_t), intent(out) :: this
    class(charged_particles_t), intent(in)  :: cp_in

    PUSH_SUB(charged_particles_copy)

    call classical_particles_copy(this, cp_in)
    SAFE_ALLOCATE_SOURCE_A(this%charge, cp_in%charge)

    POP_SUB(charged_particles_copy)
  end subroutine charged_particles_copy

  ! ---------------------------------------------------------
  subroutine charged_particles_init_interaction(this, interaction)
    class(charged_particles_t), target, intent(inout) :: this
    class(interaction_surrogate_t),     intent(inout) :: interaction

    PUSH_SUB(charged_particles_init_interaction)

    select type (interaction)
    type is (coulomb_force_t)
      call interaction%init(this%space%dim, this%np, this%charge, this%pos)
    type is (lorentz_force_t)
      call interaction%init(this%space%dim, this%np, this%charge, this%pos, this%vel, this%namespace)
    class default
      ! Other interactions should be handled by the parent class
      call classical_particles_init_interaction(this, interaction)
    end select

    POP_SUB(charged_particles_init_interaction)
  end subroutine charged_particles_init_interaction

  ! ---------------------------------------------------------
  subroutine charged_particles_update_quantity(this, label)
    class(charged_particles_t), intent(inout) :: this
    character(len=*),           intent(in)    :: label

    PUSH_SUB(charged_particles_update_quantity)

    select case (label)
    case("current")
      ! Nothing to do
    case default
      ! Other quantities should be handled by the parent class
      call classical_particles_update_quantity(this, label)
    end select

    POP_SUB(charged_particles_update_quantity)
  end subroutine charged_particles_update_quantity

  ! ---------------------------------------------------------
  subroutine charged_particles_init_interaction_as_partner(partner, interaction)
    class(charged_particles_t), intent(in)    :: partner
    class(interaction_surrogate_t), intent(inout) :: interaction

    PUSH_SUB(charged_particles_init_interaction_as_partner)

    select type (interaction)
    type is (coulomb_force_t)
      interaction%partner_np = partner%np
      SAFE_ALLOCATE(interaction%partner_charge(1:partner%np))
      SAFE_ALLOCATE(interaction%partner_pos(1:partner%space%dim, 1:partner%np))

    type is (current_to_mxll_field_t)
      interaction%partner_np = partner%np
      interaction%grid_based_partner = .false.
      SAFE_ALLOCATE(interaction%partner_charge(1:partner%np))
      SAFE_ALLOCATE(interaction%partner_pos(1:partner%space%dim, 1:partner%np))
      SAFE_ALLOCATE(interaction%partner_vel(1:partner%space%dim, 1:partner%np))

    class default
      ! Other interactions should be handled by the parent class
      call classical_particles_init_interaction_as_partner(partner, interaction)
    end select

    POP_SUB(charged_particles_init_interaction_as_partner)
  end subroutine charged_particles_init_interaction_as_partner

  ! ---------------------------------------------------------
  subroutine charged_particles_copy_quantities_to_interaction(partner, interaction)
    class(charged_particles_t),         intent(inout) :: partner
    class(interaction_surrogate_t),     intent(inout) :: interaction

    PUSH_SUB(charged_particles_copy_quantities_to_interaction)

    select type (interaction)
    type is (coulomb_force_t)
      interaction%partner_charge = partner%charge
      interaction%partner_pos = partner%pos

    type is (current_to_mxll_field_t)
      interaction%partner_charge = partner%charge
      interaction%partner_pos = partner%pos
      interaction%partner_vel = partner%vel

    class default
      ! Other interactions should be handled by the parent class
      call classical_particles_copy_quantities_to_interaction(partner, interaction)
    end select

    POP_SUB(charged_particles_copy_quantities_to_interaction)
  end subroutine charged_particles_copy_quantities_to_interaction

  ! ---------------------------------------------------------
  subroutine charged_particles_end(this)
    class(charged_particles_t), intent(inout) :: this

    PUSH_SUB(charged_particles_end)

    call classical_particles_end(this)
    SAFE_DEALLOCATE_A(this%charge)

    POP_SUB(charged_particles_end)
  end subroutine charged_particles_end

end module charged_particles_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
