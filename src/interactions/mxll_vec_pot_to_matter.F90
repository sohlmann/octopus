!! Copyright (C) 2022 F. Bonafé
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!
#include "global.h"

module mxll_vec_pot_to_matter_oct_m
  use debug_oct_m
  use field_transfer_oct_m
  use global_oct_m
  use interaction_partner_oct_m

  implicit none

  private
  public ::                    &
    mxll_vec_pot_to_matter_t

  !> @brief class to transfer a Maxwell vector potential to a medium
  !!
  !! This interaction overrides the do_mapping function to be able to also
  !! treat particles as partner system in addition to grid-based partner systems
  !! as in the field interaction class.
  !!
  !! After the field has been computed, it is added to the interpolation object.
  !!
  type, extends(field_transfer_t) :: mxll_vec_pot_to_matter_t
  contains
    final :: mxll_vec_pot_to_matter_finalize
  end type mxll_vec_pot_to_matter_t


  interface mxll_vec_pot_to_matter_t
    module procedure mxll_vec_pot_to_matter_constructor
  end interface mxll_vec_pot_to_matter_t

contains

  function mxll_vec_pot_to_matter_constructor(partner) result(this)
    class(interaction_partner_t), target, intent(inout) :: partner
    class(mxll_vec_pot_to_matter_t), pointer :: this

    PUSH_SUB(mxll_vec_pot_to_matter_constructor)

    allocate(this)

    this%label = "mxll_vec_pot_to_matter"
    this%partner => partner

    this%couplings_from_partner = ["vector potential"]

    this%intra_interaction = .false.

    POP_SUB(mxll_vec_pot_to_matter_constructor)
  end function mxll_vec_pot_to_matter_constructor

! ---------------------------------------------------------
  subroutine mxll_vec_pot_to_matter_finalize(this)
    type(mxll_vec_pot_to_matter_t), intent(inout) :: this

    PUSH_SUB(mxll_vec_pot_to_matter_finalize)

    call this%end()

    POP_SUB(mxll_vec_pot_to_matter_finalize)
  end subroutine mxll_vec_pot_to_matter_finalize

end module mxll_vec_pot_to_matter_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
