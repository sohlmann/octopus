target_sources(Octopus_lib PRIVATE
		dftb.F90
)
if (TARGET DftbPlus::DftbPlus)
	target_link_libraries(Octopus_lib PRIVATE DftbPlus::DftbPlus)
endif ()
