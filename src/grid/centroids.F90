!! Copyright (C) 2024 A. Buccheri
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.

#include "global.h"

module centroids_oct_m
  use, intrinsic :: iso_fortran_env
  use comm_oct_m,      only: comm_allreduce
  use debug_oct_m
  use global_oct_m
  use mesh_oct_m
  use messages_oct_m
  use mpi_oct_m,       only: MPI_Comm
  use profiling_oct_m

  implicit none
  private

  public :: centroids_t

  !> @brief Encapsulate centroid points and their indexing across a domain-decomposed mesh.
  !!
  !! Example usage:
  !!
  !! ```fortran
  !!  type(centroids_t) :: centroids
  !!
  !!  call centroids%init(mesh, positions)
  !!  n_centroids_global = centroids%npoints_global()
  !!  n_centroids = centroids%npoints()
  !!
  !!  ! Loop over centroid indices local to the domain on this MPI process
  !!  do ic = 1, n_centroids
  !!      ip = centroids%to_local_mesh_index(ic)
  !!      ipg = centroids%to_global_mesh_index(ic)
  !!      icg = centroids%to_global_index(ic)
  !!      ! position in this domain
  !!      x_centroid = centroid%get_local_position(mesh, ic)
  !!  enddo
  !! ```
  !!
  !! @note
  !! This could be a more generic subgrid or index class, as the class''s job is to
  !! track a subset of grid indices when domain-decomposed. One would simply need to
  !! rename everything.
  type centroids_t
    private
    integer                     :: ndim               !< System dimensions
    real(real64),   allocatable :: positions(:, :)    !< All centroid cartesian positions
    integer                     :: n_centroids_global !< Total number of centroids over the whole mesh/system
    integer                     :: n_centroids        !< Number of centroids in local domain
    integer(int64), allocatable :: icg_to_ipg(:)      !< Map global centroid index to global mesh index
    !                                                    Kept private, but one could add a getter if required
    integer,        allocatable :: ic_to_icg(:)       !< Map local centroid index to global centroid index
    integer,        allocatable :: ic_to_ip(:)        !< Map local centroid index to mesh index

  contains
    procedure :: init => centroids_init
    procedure :: npoints => centroids_get_n_centroids
    procedure :: npoints_global => centroids_get_n_centroids_global
    procedure :: get_local_position => centroids_get_local_position
    procedure :: get_all_positions => centroids_get_all_positions
    ! ic -> ip
    procedure :: to_local_mesh_index => centroids_get_local_mesh_index
    procedure :: local_mesh_indices => centroids_get_local_mesh_indices
    ! ic -> icg
    procedure :: to_global_index => centroids_local_index_to_global_index
    procedure :: global_indices => centroids_local_index_to_global_indices
    ! ic -> ipg
    procedure :: to_global_mesh_index => centroids_global_centroid_to_global_mesh_index
    procedure :: global_mesh_indices => centroids_global_centroid_to_global_mesh_indices
    final     :: centroids_finalize
  end type centroids_t

contains

  !> @brief Initialise a centroids_t instance
  !!
  !! Centroid positions must correspond to grid points.
  subroutine centroids_init(this, mesh, positions)
    class(centroids_t),  intent(out) :: this            !< Centroid instance
    class(mesh_t),       intent(in ) :: mesh            !< Mesh or grid instance
    real(real64),        intent(in ) :: positions(:, :) !< All centroid cartesian positions

    PUSH_SUB(centroids_init)

    this%ndim = size(positions, 1)
    this%n_centroids_global = size(positions, 2)
    SAFE_ALLOCATE_SOURCE(this%positions(1:this%ndim, 1:this%n_centroids_global), positions)
    call exact_centroids_init_index_maps(this, mesh)

    POP_SUB(centroids_init)

  end subroutine centroids_init


  !> @brief Number of centroids in local domain
  pure function centroids_get_n_centroids(this) result(n_centroids)
    class(centroids_t),  intent(in) :: this
    integer                         :: n_centroids

    n_centroids = this%n_centroids

  end function centroids_get_n_centroids


  !> @brief Total number of centroids in system
  pure function centroids_get_n_centroids_global(this) result(n_centroids_global)
    class(centroids_t),  intent(in) :: this
    integer                         :: n_centroids_global

    n_centroids_global = this%n_centroids_global

  end function centroids_get_n_centroids_global


  !> @brief Given centroid index \p ic, return the global mesh index \p ipg
  !!
  !! @note
  !! One could alternatively implement this with:
  !! ```fortran
  !!  ip = this%ic_to_ip(ic)
  !!  ipg = mesh_local2global(mesh, ip)
  !! ```
  !! which would remove the need to store `icg_to_ipg`, but this would require
  !! mesh to be an argument of the function.
  pure function centroids_global_centroid_to_global_mesh_index(this, ic) result(ipg)
    class(centroids_t),  intent(in) :: this
    integer,             intent(in) :: ic
    integer(int64)                  :: ipg
    integer                         :: icg

    icg = this%ic_to_icg(ic)
    ipg = this%icg_to_ipg(icg)

  end function centroids_global_centroid_to_global_mesh_index


  !> @brief Return global mesh indices for all centroids in this domain
  pure function centroids_global_centroid_to_global_mesh_indices(this) result(global_mesh_indices)
    class(centroids_t),  intent(in) :: this
    integer(int64), allocatable     :: global_mesh_indices(:)

    integer                         :: ic, icg

    allocate(global_mesh_indices(this%n_centroids))
    do ic = 1, this%n_centroids
      icg = this%ic_to_icg(ic)
      global_mesh_indices(ic) = this%icg_to_ipg(icg)
    enddo

  end function centroids_global_centroid_to_global_mesh_indices


  !> @brief Get the mesh index `ip` of centroid \p ic in this domain
  pure function centroids_get_local_mesh_index(this, ic) result(ip)
    class(centroids_t),  intent(in) :: this
    integer,             intent(in) :: ic
    integer                         :: ip

    ip = this%ic_to_ip(ic)

  end function centroids_get_local_mesh_index


  !> @brief Return mesh indices `ip` for all centroids in this domain
  pure function centroids_get_local_mesh_indices(this) result(mesh_indices)
    class(centroids_t),  intent(in) :: this
    integer, allocatable            :: mesh_indices(:)

    allocate(mesh_indices(this%n_centroids), source=this%ic_to_ip)

  end function centroids_get_local_mesh_indices


  !> @brief Get the global centroid index, given centroid \p ic this domain
  pure function centroids_local_index_to_global_index(this, ic) result(icg)
    class(centroids_t),  intent(in) :: this
    integer,             intent(in) :: ic
    integer                         :: icg

    icg = this%ic_to_icg(ic)

  end function centroids_local_index_to_global_index


  !> @brief Return global centroid indices for all centroids in this domain
  pure function centroids_local_index_to_global_indices(this) result(global_centroid_indices)
    class(centroids_t),  intent(in) :: this
    integer, allocatable            :: global_centroid_indices(:)

    allocate(global_centroid_indices(this%n_centroids), source=this%ic_to_icg)

  end function centroids_local_index_to_global_indices


  !> @brief Get the position of the centroid \p ic
  pure function centroids_get_local_position(this, mesh, ic) result(pos)
    class(centroids_t),  intent(in) :: this
    class(mesh_t),       intent(in) :: mesh
    integer,             intent(in) :: ic

    real(real64)                    :: pos(this%ndim)

    pos(:) = mesh%x(this%ic_to_ip(ic), :)

  end function centroids_get_local_position


  !> @brief Return a copy of all centroid positions
  pure function centroids_get_all_positions(this) result(positions)
    class(centroids_t),  intent(in) :: this
    real(real64), allocatable :: positions(:, :)

    allocate(positions(this%ndim, this%n_centroids_global), source=this%positions)

  end function centroids_get_all_positions


  !> @brief Finalize a centroids instance
  subroutine centroids_finalize(this)
    type(centroids_t),  intent(inout) :: this !< Centroid instance

    SAFE_DEALLOCATE_A(this%positions)
    SAFE_DEALLOCATE_A(this%icg_to_ipg)
    SAFE_DEALLOCATE_A(this%ic_to_icg)

  end subroutine centroids_finalize

  ! Helper function for initialisation, hence not type-bound or exposed

  !> @brief Initialise index maps with centroid points that are commensurate with
  !! the grid.
  !!
  !! This routine breaks single responsibility, but it makes sense to reuse the
  !! the various indices as they are computed.
  !! This routine computes:
  !! * Convert centroid positions to global mesh index, \p ipg
  !! * Set number of centroids in the domain of \p process
  !! * Map global centroid index \p icg to centroid index in domain \p process
  !! * Map centroid index \p ic in domain \p process, to mesh index \p ip in the same domain
  subroutine exact_centroids_init_index_maps(this, mesh)
    type(centroids_t),   intent(inout) :: this
    class(mesh_t),       intent(in   ) :: mesh   !< Mesh or grid instance

    real(real64)                       :: chi(this%ndim)
    integer                            :: ix(this%ndim)
    integer                            :: icg, ip
    integer(int64)                     :: ipg
    integer, allocatable               :: tmp_ic_to_icg(:), tmp_ic_to_ip(:)
#if !defined(NDEBUG)
    integer                            :: n_centroids_global
#endif

    PUSH_SUB(exact_centroids_init_index_maps)

    SAFE_ALLOCATE(this%icg_to_ipg(1:this%n_centroids_global))

    ! Local to this process
    this%n_centroids = 0
    ! Allocated with an upper bound
    SAFE_ALLOCATE(tmp_ic_to_icg(1:this%n_centroids_global))
    SAFE_ALLOCATE(tmp_ic_to_ip(1:this%n_centroids_global))

    do icg = 1, this%n_centroids_global
      chi = mesh%coord_system%from_cartesian(this%positions(:, icg))
      ! See the definition of mesh_x_global, for example
      ix = nint(chi / mesh%spacing)
      ipg = mesh_global_index_from_coords(mesh, ix)
      this%icg_to_ipg(icg) = ipg
      ! Domain-specific indices
      ip = mesh_global2local(mesh, ipg)
      if (ip > 0 .and. ip <= mesh%np) then
        this%n_centroids = this%n_centroids + 1
        tmp_ic_to_icg(this%n_centroids) = icg
        tmp_ic_to_ip(this%n_centroids) = ip
      endif
    enddo

#if !defined(NDEBUG)
    ! Total number of centroids must be conserved over all domains
    n_centroids_global = this%n_centroids
    call comm_allreduce(mesh%mpi_grp, n_centroids_global)
    ASSERT(n_centroids_global == this%n_centroids_global)
#endif

    ! Reallocate with correct size
    SAFE_ALLOCATE_SOURCE(this%ic_to_icg(1:this%n_centroids), tmp_ic_to_icg(1:this%n_centroids))
    SAFE_DEALLOCATE_A(tmp_ic_to_icg)

    SAFE_ALLOCATE_SOURCE(this%ic_to_ip(1:this%n_centroids), tmp_ic_to_ip(1:this%n_centroids))
    SAFE_DEALLOCATE_A(tmp_ic_to_ip)

    POP_SUB(exact_centroids_init_index_maps)

  end subroutine exact_centroids_init_index_maps

end module centroids_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
