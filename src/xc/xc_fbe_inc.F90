!! Copyright (C) 2021 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!
!!
! ---------------------------------------------------------
subroutine X(x_fbe_calc)(namespace, psolver, mesh, der, st, ex, vxc, fxc)
  type(namespace_t),        intent(in)    :: namespace
  type(poisson_t),          intent(in)    :: psolver
  class(mesh_t),            intent(in)    :: mesh
  type(derivatives_t),      intent(in)    :: der
  type(states_elec_t),      intent(inout) :: st
  real(real64),             intent(inout) :: ex
  real(real64), contiguous, optional, intent(inout) :: vxc(:,:) !< vxc(gr%mesh%np, st%d%nspin)
  real(real64), contiguous, optional, intent(out)   :: fxc(:,:,:) !< the exchange force density

  real(real64) :: eig
  real(real64), allocatable :: internal_vxc(:,:)

  PUSH_SUB_WITH_PROFILE(X(x_fbe_calc))

  ASSERT(st%d%ispin /= SPINORS)
  !At the moment we treat only spins and not k-points
  ASSERT(st%nik == st%d%spin_channels)

  if (present(vxc)) then
    SAFE_ALLOCATE(internal_vxc(1:mesh%np, 1:st%d%spin_channels))
    internal_vxc = M_ZERO
  end if
  if (present(fxc)) then
    fxc = M_ZERO
  end if

  eig = M_ZERO
  if (present(vxc)) then
    call X(fbe_x)(namespace, mesh, der, psolver, st, eig, vxc=internal_vxc, fxc=fxc)
  else
    call X(fbe_x)(namespace, mesh, der, psolver, st, eig, fxc=fxc)
  end if

  ! Reduction over spin channels
  if (st%d%kpt%parallel) then
    call comm_allreduce(st%d%kpt%mpi_grp, eig)
    if (present(vxc)) call comm_allreduce(st%d%kpt%mpi_grp, internal_vxc)
    if (present(fxc)) call comm_allreduce(st%d%kpt%mpi_grp, fxc)
  end if

  ex = ex + eig
  if (present(vxc)) then
    call lalg_axpy(mesh%np, st%d%spin_channels, M_ONE, internal_vxc, vxc)
  end if

  SAFE_DEALLOCATE_A(internal_vxc)

  POP_SUB_WITH_PROFILE(X(x_fbe_calc))
end subroutine X(x_fbe_calc)

!------------------------------------------------------------
!>@brief Computes the FBEx potential and possibly force
!!
!! Note that this routine is adapted from the X(slater) routine
subroutine X(fbe_x) (namespace, mesh, der, psolver, st, ex, vxc, fxc)
  type(namespace_t),           intent(in)    :: namespace
  class(mesh_t),               intent(in)    :: mesh
  type(derivatives_t),         intent(in)    :: der
  type(poisson_t),             intent(in)    :: psolver
  type(states_elec_t),         intent(inout) :: st
  real(real64),                intent(inout) :: ex
  real(real64), contiguous, optional, intent(inout) :: vxc(:,:) !< vxc(gr%mesh%np, st%d%spin_channels)
  real(real64), contiguous, optional, intent(inout) :: fxc(:,:,:) !< the exchange force density

  integer :: ii, ib, ik, isp, nsteps, node_to, node_fr, idm, ip, nreceiv, icom
  type(ring_pattern_t) :: ring_pattern
  type(wfs_elec_t) :: psib_receiv
  real(real64), allocatable :: tmp_vxc(:,:,:), tmp_pot(:,:,:), div(:,:), grad_rho(:,:,:), slater(:,:)
  type(states_elec_all_to_all_communications_t) :: alltoallcomm
  real(real64) :: ex_

  PUSH_SUB(X(fbe_x))

  ASSERT(mesh%box%dim == 3) ! The present code is only in 3D, as otherwise the Poisson equation is different

  if (present(vxc) .or. present(fxc)) then
    SAFE_ALLOCATE(tmp_pot(1:mesh%np, 1:mesh%box%dim, 1:st%d%spin_channels))
    SAFE_ALLOCATE(tmp_vxc(1:mesh%np_part, 1:mesh%box%dim, 1:st%d%spin_channels))
    tmp_vxc = M_ZERO
    SAFE_ALLOCATE(slater(1:mesh%np_part, 1:st%d%spin_channels))
    slater = M_ZERO
    !We need the gradient of the density to evaluate the potential
    SAFE_ALLOCATE(grad_rho(1:mesh%np, 1:mesh%box%dim, 1:st%d%spin_channels))
    do isp = 1, st%d%spin_channels
      call dderivatives_grad(der, st%rho(:, isp), grad_rho(:,:,isp))
    end do
  end if

  ex_ = M_ZERO

  !We start by the contribution from states all present in memory
  do ik = st%d%kpt%start, st%d%kpt%end
    do ib = st%group%block_start, st%group%block_end
      call X(local_contribution)(namespace, mesh, der, psolver, st, st%group%psib(ib, ik), grad_rho, ex_, &
        .true., slater, tmp_vxc)
    end do !ib
  end do !ik

  ! This is the maximum number of blocks for each processor
  call ring_pattern%start(st%st_kpt_mpi_grp, .true.)
  nsteps = ring_pattern%get_nsteps()

  do ii = 1, nsteps
    node_to = ring_pattern%get_rank_to(ii)
    node_fr = ring_pattern%get_rank_from(ii)

    call alltoallcomm%start(st, node_fr, node_to)

    !Sending local wfn to node_to
    call alltoallcomm%X(post_all_mpi_isend)(st, mesh%np, node_to)

    nreceiv = alltoallcomm%get_nreceiv()

    do icom = 1, nreceiv
      if (node_fr == -1) cycle

      !Receiving a wf from node_fr
      call alltoallcomm%X(mpi_recv_batch)(st, mesh%np, node_fr, icom, psib_receiv)

      !For each wf we received, we compute the potentials
      !using the local wfn
      call X(local_contribution)(namespace, mesh, der, psolver, st, psib_receiv, grad_rho, ex_, &
        .false., slater, tmp_vxc)
      call psib_receiv%end()

    end do

    call alltoallcomm%wait_all_isend(st)
  end do !ii

  if (st%parallel_in_states .or. st%d%kpt%parallel) then
    if (present(vxc) .or. present(fxc)) then
      call comm_allreduce(st%st_kpt_mpi_grp, tmp_vxc)
      call comm_allreduce(st%st_kpt_mpi_grp, slater)
    end if
    call comm_allreduce(st%st_kpt_mpi_grp, ex_)
  end if
  if (mesh%parallel_in_domains) call mesh%allreduce(ex_)
  ex = ex + ex_

  !First we add the part from the Slater potential
  if (present(vxc)) then
    call lalg_axpy(mesh%np, st%d%spin_channels, M_ONE, slater, vxc)
    SAFE_ALLOCATE(div(1:mesh%np_part, 1:st%d%spin_channels))
    do isp = 1, st%d%spin_channels
      call dderivatives_div(der, tmp_vxc(:, :, isp), div(:, isp))
    end do
  end if

  ! We add the non-Slater part of the force density
  if(present(fxc)) then
    !$omp parallel
    do isp = 1, st%d%spin_channels
      do idm = 1, mesh%box%dim
        !$omp do
        do ip = 1, mesh%np
          fxc(ip, idm, isp) = tmp_vxc(ip, idm, isp)*st%rho(ip, isp)
        end do
        !$omp end do nowait
      end do
    end do
    !$omp end parallel
  end if

  ! We add the non-Slater part to the potential
  if (present(vxc)) then
    do isp = 1, st%d%spin_channels
      tmp_vxc(:, 1, 1) = M_ZERO
      call dpoisson_solve(psolver, namespace, tmp_vxc(:, 1, 1), div(:, isp), all_nodes=.true.)
      call lalg_axpy(mesh%np, M_ONE / M_PI / M_FOUR, tmp_vxc(:, 1, 1), vxc(:, isp))
    end do
    SAFE_DEALLOCATE_A(div)
  end if

  if(present(fxc)) then
    ! We compute the gradient of Slater and add it
    do isp = 1, st%d%spin_channels
      call dderivatives_grad(der, slater(:, isp), tmp_vxc(:,:,isp))
    end do
    !$omp parallel
    do isp = 1, st%d%spin_channels
      do idm = 1, mesh%box%dim
        !$omp do
        do ip = 1, mesh%np
          fxc(ip, idm, isp) = -fxc(ip, idm, isp) + tmp_vxc(ip, idm, isp)*st%rho(ip, isp)
        end do
        !$omp end do nowait
      end do
    end do
    !$omp end parallel
  end if
  SAFE_DEALLOCATE_A(tmp_vxc)
  SAFE_DEALLOCATE_A(grad_rho)
  SAFE_DEALLOCATE_A(slater)

  POP_SUB(X(fbe_x))
end subroutine X(fbe_x)

!>@brief Given a communicated batch, compute its contribution to the total quantities
subroutine X(local_contribution)(namespace, mesh, der, psolver, st, psib_receiv, grad_rho, ex_, local, slater, tmp_vxc)
  type(namespace_t),                  intent(in)    :: namespace
  class(mesh_t),                      intent(in)    :: mesh
  type(derivatives_t),                intent(in)    :: der
  type(poisson_t),                    intent(in)    :: psolver
  type(states_elec_t),                intent(inout) :: st
  class(wfs_elec_t),                  intent(in)    :: psib_receiv
  real(real64), contiguous,           intent(in)    :: grad_rho(:,:,:)
  real(real64),                       intent(inout) :: ex_
  logical,                            intent(in)    :: local
  real(real64), allocatable,          intent(inout) :: slater(:,:)
  real(real64), allocatable,          intent(inout) :: tmp_vxc(:,:,:)

  R_TYPE, allocatable :: rho_ij(:), pot_ij(:), psi(:,:), wf_ist(:,:), grad_rho_ij(:,:)
  integer :: ibatch, ik2, ispin, ip, ist, jst, idm, ik
  real(real64) :: rr, tmp

  PUSH_SUB(X(local_contribution))

  ik = psib_receiv%ik
  ispin = st%d%get_spin_index(ik)

  ASSERT(allocated(slater) .eqv. allocated(tmp_vxc))

  SAFE_ALLOCATE(pot_ij(1:mesh%np))
  SAFE_ALLOCATE(grad_rho_ij(1:mesh%np, 1:mesh%box%dim))
  SAFE_ALLOCATE(rho_ij(1:mesh%np_part))
  SAFE_ALLOCATE(psi(1:mesh%np, 1:st%d%dim))
  SAFE_ALLOCATE(wf_ist(1:mesh%np, 1:st%d%dim))

  ! We loop over the received batch of states
  do ibatch = 1, psib_receiv%nst
    ist = psib_receiv%ist(ibatch)
    call batch_get_state(psib_receiv, ibatch, mesh%np, wf_ist)

    do ik2 = st%d%kpt%start, st%d%kpt%end

      if (st%d%get_spin_index(ik2) /= ispin) cycle

      do jst = st%st_start, st%st_end

        rr = st%kweights(ik)*st%occ(ist, ik)*st%occ(jst, ik2)/st%smear%el_per_state
        if (abs(rr) <= M_EPSILON) cycle

        if (jst < ist .and. local) cycle
        if (ist /= jst .or. ik /= ik2) rr = rr * M_TWO

        call states_elec_get_state(st, mesh, jst, ik2, psi)
        rho_ij(1:mesh%np) = R_CONJ(wf_ist(1:mesh%np, 1))*psi(1:mesh%np, 1)

        call X(poisson_solve)(psolver, namespace, pot_ij, rho_ij, all_nodes=.false.)


        ! Here we accumulate the result for the potential
        if (allocated(slater)) then
          ! There are two contributitions, the gradient of the Slater potential, and the
          ! Poisson equation with the gradient of the exchange hole

          ! We first compute the Slater contribution
          !$omp parallel do
          do ip = 1, mesh%np
            !If there is no density at this point, we simply ignore it
            slater(ip, ispin) = slater(ip, ispin) - rr * R_REAL(R_CONJ(rho_ij(ip)) * pot_ij(ip)) &
              / (SAFE_TOL(st%rho(ip, ispin), M_EPSILON))
          end do

          call X(derivatives_grad)(der, rho_ij(:), grad_rho_ij(:, :))

          !$omp parallel private(tmp, ip)
          do idm = 1, mesh%box%dim
            !$omp do
            do ip = 1, mesh%np
              ! For numerical reasons, we need to compute the gradient of the \rho_ij/n this way
              ! else, we have severe numerical troubles at the border of the simulation box
              ! especially in the case of one electron only (\rho_ij = n)
              tmp = R_REAL(R_CONJ(grad_rho_ij(ip, idm)) * pot_ij(ip)) * st%rho(ip, ispin) &
                -R_REAL(R_CONJ(rho_ij(ip)) * pot_ij(ip)) * grad_rho(ip, idm, ispin)

              tmp_vxc(ip, idm, ispin) = tmp_vxc(ip, idm, ispin)  - rr * tmp &
                / (SAFE_TOL(st%rho(ip, ispin)**2, M_EPSILON**2))
            end do
            !$omp end do nowait
          end do
          !$omp end parallel
        end if

        ! not an energy functional
        ! get the contribution to the exchange energy
        ex_ = ex_ - M_HALF * rr * R_REAL(X(mf_dotp)(mesh, rho_ij, pot_ij, reduce=.false.))
      end do ! jst
    end do ! ik2
  end do ! ibatch

  SAFE_DEALLOCATE_A(pot_ij)
  SAFE_DEALLOCATE_A(rho_ij)
  SAFE_DEALLOCATE_A(psi)
  SAFE_DEALLOCATE_A(wf_ist)

  POP_SUB(X(local_contribution))
end subroutine X(local_contribution)

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
