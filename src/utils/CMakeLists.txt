add_executable(Octopus_Utils_casida_spectrum casida_spectrum.F90)
set_target_properties(Octopus_Utils_casida_spectrum PROPERTIES
		OUTPUT_NAME oct-casida_spectrum)
target_link_libraries(Octopus_Utils_casida_spectrum PRIVATE
		Octopus_base Octopus_lib fortran_cli)

add_executable(Octopus_Utils_center-geom centergeom.F90)
set_target_properties(Octopus_Utils_center-geom PROPERTIES
		OUTPUT_NAME oct-center-geom)
target_link_libraries(Octopus_Utils_center-geom PRIVATE
		Octopus_base Octopus_lib fortran_cli)

add_executable(Octopus_Utils_conductivity conductivity.F90)
set_target_properties(Octopus_Utils_conductivity PROPERTIES
		OUTPUT_NAME oct-conductivity)
target_link_libraries(Octopus_Utils_conductivity PRIVATE
		Octopus_base Octopus_lib fortran_cli)

add_executable(Octopus_Utils_convert convert.F90)
set_target_properties(Octopus_Utils_convert PROPERTIES
		OUTPUT_NAME oct-convert)
target_link_libraries(Octopus_Utils_convert PRIVATE
		Octopus_base Octopus_lib fortran_cli)

add_executable(Octopus_Utils_dielectric-function dielectric_function.F90)
set_target_properties(Octopus_Utils_dielectric-function PROPERTIES
		OUTPUT_NAME oct-dielectric-function)
target_link_libraries(Octopus_Utils_dielectric-function PRIVATE
		Octopus_base Octopus_lib fortran_cli)

add_executable(Octopus_Utils_floquet floquet.F90)
set_target_properties(Octopus_Utils_floquet PROPERTIES
		OUTPUT_NAME oct-floquet)
target_link_libraries(Octopus_Utils_floquet PRIVATE
		Octopus_base Octopus_lib fortran_cli)
if (TARGET MKL::MKL)
	target_link_libraries(Octopus_Utils_floquet PRIVATE MKL::MKL)
else ()
	target_link_libraries(Octopus_Utils_floquet PRIVATE BLAS::BLAS LAPACK::LAPACK)
endif ()

add_executable(Octopus_Utils_harmonic-spectrum harmonic_spectrum.F90)
set_target_properties(Octopus_Utils_harmonic-spectrum PROPERTIES
		OUTPUT_NAME oct-harmonic-spectrum)
target_link_libraries(Octopus_Utils_harmonic-spectrum PRIVATE
		Octopus_base Octopus_lib fortran_cli)

add_executable(Octopus_Utils_help help.F90)
set_target_properties(Octopus_Utils_help PROPERTIES
		OUTPUT_NAME oct-help)
target_link_libraries(Octopus_Utils_help PRIVATE
		Octopus_base Octopus_lib fortran_cli)

add_executable(Octopus_Utils_infrared_spectrum infrared.F90)
set_target_properties(Octopus_Utils_infrared_spectrum PROPERTIES
		OUTPUT_NAME oct-infrared_spectrum)
target_link_libraries(Octopus_Utils_infrared_spectrum PRIVATE
		Octopus_base Octopus_lib fortran_cli)
if (TARGET OpenMP::OpenMP_Fortran)
	target_link_libraries(Octopus_Utils_infrared_spectrum PRIVATE OpenMP::OpenMP_Fortran)
	target_compile_definitions(Octopus_Utils_infrared_spectrum PRIVATE HAVE_OPENMP)
endif ()

add_executable(Octopus_Utils_local_multipoles local_multipoles.F90)
set_target_properties(Octopus_Utils_local_multipoles PROPERTIES
		OUTPUT_NAME oct-local_multipoles)
target_sources(Octopus_Utils_local_multipoles PRIVATE
		local_write.F90)
target_link_libraries(Octopus_Utils_local_multipoles PRIVATE
		Octopus_base Octopus_lib fortran_cli)
if (TARGET OpenMP::OpenMP_Fortran)
	target_link_libraries(Octopus_Utils_local_multipoles PRIVATE OpenMP::OpenMP_Fortran)
	target_compile_definitions(Octopus_Utils_local_multipoles PRIVATE HAVE_OPENMP)
endif ()

add_executable(Octopus_Utils_oscillator-strength oscillator_strength.F90)
set_target_properties(Octopus_Utils_oscillator-strength PROPERTIES
		OUTPUT_NAME oct-oscillator-strength)
target_link_libraries(Octopus_Utils_oscillator-strength PRIVATE
		Octopus_base Octopus_lib fortran_cli)

add_executable(Octopus_Utils_photoelectron_spectrum photoelectron_spectrum.F90)
set_target_properties(Octopus_Utils_photoelectron_spectrum PROPERTIES
		OUTPUT_NAME oct-photoelectron_spectrum)
target_link_libraries(Octopus_Utils_photoelectron_spectrum PRIVATE
		Octopus_base Octopus_lib fortran_cli)

add_executable(Octopus_Utils_propagation_spectrum propagation_spectrum.F90)
set_target_properties(Octopus_Utils_propagation_spectrum PROPERTIES
		OUTPUT_NAME oct-propagation_spectrum)
target_link_libraries(Octopus_Utils_propagation_spectrum PRIVATE
		Octopus_base Octopus_lib fortran_cli)

add_executable(Octopus_Utils_spin_susceptibility spin_susceptibility.F90)
set_target_properties(Octopus_Utils_spin_susceptibility PROPERTIES
		OUTPUT_NAME oct-spin_susceptibility)
target_link_libraries(Octopus_Utils_spin_susceptibility PRIVATE
		Octopus_base Octopus_lib fortran_cli)

add_executable(Octopus_Utils_tdtdm tdtdm.F90)
set_target_properties(Octopus_Utils_tdtdm PROPERTIES
		OUTPUT_NAME oct-tdtdm)
target_link_libraries(Octopus_Utils_tdtdm PRIVATE
		Octopus_base Octopus_lib fortran_cli)
if (TARGET MPI::MPI_Fortran)
	target_link_libraries(Octopus_lib PUBLIC MPI::MPI_Fortran)
endif ()

add_executable(Octopus_Utils_unfold unfold.F90)
set_target_properties(Octopus_Utils_unfold PROPERTIES
		OUTPUT_NAME oct-unfold)
target_link_libraries(Octopus_Utils_unfold PRIVATE
		Octopus_base Octopus_lib fortran_cli)

add_executable(Octopus_Utils_vibrational_spectrum vibrational.F90)
set_target_properties(Octopus_Utils_vibrational_spectrum PROPERTIES
		OUTPUT_NAME oct-vibrational_spectrum)
target_link_libraries(Octopus_Utils_vibrational_spectrum PRIVATE
		Octopus_base Octopus_lib fortran_cli)

add_executable(Octopus_Utils_wannier90 wannier90_interface.F90)
set_target_properties(Octopus_Utils_wannier90 PROPERTIES
		OUTPUT_NAME oct-wannier90)
target_sources(Octopus_Utils_wannier90 PRIVATE
		../math/ylm_wannier.F90)
target_link_libraries(Octopus_Utils_wannier90 PRIVATE
		Octopus_base Octopus_lib fortran_cli)

add_executable(Octopus_Utils_xyz-anim xyzanim.F90)
set_target_properties(Octopus_Utils_xyz-anim PROPERTIES
		OUTPUT_NAME oct-xyz-anim)
target_link_libraries(Octopus_Utils_xyz-anim PRIVATE
		Octopus_base Octopus_lib fortran_cli)

get_directory_property(UtilTargets BUILDSYSTEM_TARGETS)
set_target_properties(${UtilTargets} PROPERTIES
		LINKER_LANGUAGE Fortran)
if (OCTOPUS_INSTALL)
	install(TARGETS ${UtilTargets}
			RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
endif ()

# TODO: Remove hardcoded file structure in testsuite
foreach(oct_exec IN ITEMS ${UtilTargets})
	add_custom_command(TARGET ${oct_exec} POST_BUILD
			COMMAND mkdir -p ${PROJECT_BINARY_DIR}
			COMMAND ln -f -s $<TARGET_FILE:${oct_exec}> ${PROJECT_BINARY_DIR}/$<TARGET_FILE_NAME:${oct_exec}>
			COMMENT "Generate default symbolic link to ${oct_exec} to default build directory")
endforeach()
