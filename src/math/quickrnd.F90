!! Copyright (C) 2002-2024 M. Marques, A. Castro, A. Rubio, G. Bertsch, X. Andrade, A. Buccheri, H. Menke
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module quickrnd_oct_m
  use, intrinsic :: iso_fortran_env
  use debug_oct_m
  use global_oct_m
  use messages_oct_m
  use profiling_oct_m

  implicit none

  private ::                    &
    xorshift64,                 &
    to_double
  public ::                     &
    quickrnd,                   &
    shiftseed,                  &
    reservoir_sampling_aexpj,   &
    fisher_yates_shuffle

  ! xorshift64 needs a good seed, in particular it cannot be seeded with 0.
  ! These seeds are generated with the splitmix64 generator and have a
  ! reasonable amount of entropy.
  integer(int64), parameter, public ::                &
    splitmix64_123 = int(Z'B4DC9BD462DE412B', int64), & ! splitmix64(123)
    splitmix64_321 = int(Z'E95F1C4EFCF85DEE', int64)    ! splitmix64(321)

  interface quickrnd
    module procedure dquickrnd_single, dquickrnd_array, zquickrnd_array
  end interface quickrnd

contains

  !> @brief xorshift64 pseudorandom number generator
  !!
  !! Marsaglia, G. (2003). Xorshift RNGs. Journal of Statistical Software, 8(14).
  !! https://doi.org/10.18637/jss.v008.i14
  function xorshift64(x) result(res)
    integer(int64), intent(inout) :: x !< PRNG state
    integer(int64) :: res !< next random number in the sequence

    ASSERT(x /= 0_int64)

    x = ieor(x, shiftl(x, 13))
    x = ieor(x, shiftr(x, 7))
    x = ieor(x, shiftl(x, 17))

    res = x
  end function xorshift64

  !> @brief Generating uniform doubles in the unit interval
  !!
  !! We consider @p x to be an unsigned 64-bit integer which can represent the
  !! range \f$[0,2^{64})\f$.  First we shift @p x right by 11 places which puts
  !! it in the range \f$[0,2^{53})\f$. Then we convert it to a double and
  !! multiply by \f$2^{-53}\f$ which puts the result in the range \f$[0,1)\f$.
  !!
  !! https://prng.di.unimi.it/#remarks&:~:text=Generating%20uniform%20doubles%20in%20the%20unit%20interval
  function to_double(x) result(d)
    integer(int64), intent(in) :: x !< 64-bit integer
    real(real64) :: d !< double precision floating point number in the interval [0,1)
    real(real64), parameter :: two_pow_minus_53 = real(Z'3CA0000000000000', real64)

    d = real(shiftr(x, 11), real64) * two_pow_minus_53
  end function to_double

  ! ---------------------------------------------------------

  subroutine dquickrnd_single(iseed, rnd)
    integer(int64), intent(inout) :: iseed
    real(real64), intent(out) :: rnd

    ! no PUSH_SUB, called too often

    rnd = to_double(xorshift64(iseed))

  end subroutine dquickrnd_single

  ! ---------------------------------------------------------

  subroutine dquickrnd_array(iseed, nn, rnd)
    integer(int64), intent(inout) :: iseed
    integer, intent(in) :: nn
    real(real64), intent(inout) :: rnd(:)

    integer :: ii

    PUSH_SUB(quickrnd_array)

    do ii = 1, nn
      rnd(ii) = to_double(xorshift64(iseed))
    end do

    POP_SUB(quickrnd_array)

  end subroutine dquickrnd_array

  ! ---------------------------------------------------------

  subroutine zquickrnd_array(iseed, nn, rnd)
    integer(int64), intent(inout) :: iseed
    integer, intent(in) :: nn
    complex(real64), intent(inout) :: rnd(:)

    real(real64), parameter :: inv_sqrt2 = 1.0_real64 / sqrt(M_TWO)
    real(real64) :: re, im
    integer :: ii

    PUSH_SUB(quickrnd_array)

    do ii = 1, nn
      re = to_double(xorshift64(iseed))
      im = to_double(xorshift64(iseed))
      rnd(ii) = cmplx(re, im, real64) * inv_sqrt2
    end do

    POP_SUB(quickrnd_array)

  end subroutine zquickrnd_array

  ! ---------------------------------------------------------

  subroutine shiftseed(iseed, n)
    integer(int64), intent(inout) :: iseed
    integer(int64), intent(in) :: n

    integer(int64) :: discard
    integer(int64) :: ii

    PUSH_SUB(shiftseed)

    do ii = 1, n
      discard = xorshift64(iseed)
    end do

    POP_SUB(shiftseed)

  end subroutine shiftseed


  ! TODO(Alex) Issue #1074 . When MR !2523 is merged, scrap `xorshift_rnd_32` and `random_real_in_range`
  ! in favour of functions introduced. Use them to implement `random_number_in_range`, as described in the
  ! issue.
  subroutine xorshift_rnd_32_biased(x)
    integer(int32), intent(inout) :: x

    x = ieor(x, ishft(x,  13_int32))  ! x ^= x << 13
    x = ieor(x, ishft(x, -17_int32))  ! x ^= x >> 17
    x = ieor(x, ishft(x,   5_int32))  ! x ^= x << 5

    ! Ensure the highest bit is zero.
    ! This keeps the result in the range [0, 2^31 - 1] (positive) BUT biases against zero
    x = iand(x, Z'7FFFFFFF')

  end subroutine xorshift_rnd_32_biased


  !> @brief Generate a random real64 in the range \f$ [x_{min}, x_{max}] \f$.
  function random_real_in_range(x_min, x_max, seed) result(rand)
    real(real64),   intent(in   )  :: x_min, x_max
    integer(int32), intent(inout)  :: seed
    real(real64)    :: rand

    real(real64), parameter :: max_value_xorshift_32 = 2147483647_real64 !< 2^31 - 1
    real(real64) :: scale

    call xorshift_rnd_32_biased(seed)
    scale = real(seed, real64) / max_value_xorshift_32
    rand = scale * (x_max - x_min) + x_min

  end function random_real_in_range


  ! TODO Alex. Issue #1074 Review the use of this function
  !> @brief Generate a random int64 in the range \f$ [x_{min}, x_{max}] \f$.
  function random_integer_in_range(x_min, x_max, seed) result(rand)
    integer(int64), intent(in   ) :: x_min, x_max  !< range [x_min, x_max]
    integer(int64), intent(inout) :: seed          !< integer seed
    integer(int64)                :: rand          !< integer in the interval [x_min, x_max]

    integer(int64) :: range

    seed = xorshift64(seed)
    ! Is this is required?
    if (seed < 0_int64) seed = -seed  ! Enforce a positive result
    range = x_max - x_min + 1_int64
    rand = x_min + mod(seed, range)

  end function random_integer_in_range


  !> @brief Return \p m random numbers from a range of \f$[1, n]\f$
  !! with no replacement.
  !!
  !! The simple implementation is O(n) in memory. More details can be
  !! found on the [wikipedia page](https://en.wikipedia.org/wiki/Fisher–Yates_shuffle)
  subroutine fisher_yates_shuffle(m, n, seed, values)
    integer(int32), intent(in   ) :: m          !< Subset of random numbers to sample
    integer(int64), intent(in   ) :: n          !< Max random integer
    integer(int64), intent(inout) :: seed       !< Initial seed
    integer(int64), intent(  out) :: values(:)

    integer(int64), allocatable :: indices(:)
    integer(int64) :: i, j, temp

    PUSH_SUB(fisher_yates_shuffle)

    SAFE_ALLOCATE(indices(1:n))
    do j = 1, n
      indices(j) = j
    end do

    do i = 1, m
      j = random_integer_in_range(i, n, seed)
      ! Perform the swap
      temp = indices(i)
      indices(i) = indices(j)
      indices(j) = temp
      values(i) = indices(i)
    end do

    values(m) = indices(m)
    SAFE_DEALLOCATE_A(indices)

    POP_SUB(fisher_yates_shuffle)

  end subroutine fisher_yates_shuffle


  !> Algorithm "A-ExpJ" weighted reservoir sampling without replacement.
  !!
  !! Randomly sample a reservoir of \f$m\f$ values from a population of \f$[1, n]\f$ integers, with associated weights.
  !! Algorithm A-ExpJ is described in [Weighted random sampling with a reservoir](10.1016/j.ipl.2005.11.003)
  !! and the [wikipedia page](https://en.wikipedia.org/wiki/Reservoir_sampling#Algorithm_A-Res), purportedly scaling
  !! as \f$mlog(m/n)\f$.
  !!
  !! ## Input Weights
  !! The key function, \f$ u_i^{1/w_i} \f$, blows up for small weights, \f$\{w_i\}\f$.
  !! It is the caller''s responsibility to ensure that correctly scaled, finite weights \f$\ge 1\f$ are
  !! supplied to the routine.
  !! Rescaling could be avoided by implementing a different key function, however one would need to consistently modify
  !! the jump function \f$X_w\f$.
  !!
  !! ## Return Value
  !! Rather than returning a reservoir of \f$m\f$ values, \f${V}\f$, this implementation returns
  !! the corresponding indices of these values. The caller can use these indices to retrieve
  !! the corresponding values like `selected_values = values(reservoir)`.
  subroutine reservoir_sampling_aexpj(weight, reservoir, seed_value)
    real(real64),   intent(in )           :: weight(:)    !< Weights for the population
    integer(int32), intent(out)           :: reservoir(:) !< m randomly-sampled indices from the population
    integer(int32), intent(in ), optional :: seed_value   !< Initial seed value for PRNG

    integer(int32) :: m                  !< Sample (reservoir) size
    integer(int32) :: n                  !< Population size
    real(real64)   :: u_i, u_i2          !< Uniformly-distributed random numbers
    real(real64)   :: Xw                 !< Jump function
    real(real64), allocatable :: key(:)  !< Key for each reservoir value

    integer(int32)              :: min_key_index, i, seed_size
    integer(int32), allocatable :: seed(:)
    real(real64)                :: thres_w

    ! Random number range must be (0, 1), as log(0) would result in a key of -inf, and log(1) = 0
    ! Set range to [a, b], chosen empirically (may not be optimal)
    real(real64), parameter :: a = 1.e-10_real64
    real(real64), parameter :: b = 1._real64 - 1.e-10_real64

    PUSH_SUB(reservoir_sampling_aexpj)

    m = size(reservoir)
    n = size(weight)
    ! Cannot ask for more numbers than are present in the population
    ASSERT(m <= n)

    if (present(seed_value)) then
      SAFE_ALLOCATE_SOURCE(seed(1:1), seed_value)
    else
      ! Fortran intrinsic returns an array of seeds
      call random_seed(size=seed_size)
      SAFE_ALLOCATE(seed(1:seed_size))
      call random_seed(get=seed)
    end if

    ! Initialise the reservoir with the first m items of the population
    min_key_index = 1
    SAFE_ALLOCATE(key(1:m))

    do i = 1, m
      reservoir(i) = i
      u_i = random_real_in_range(a, b, seed(1))
      key(i) = u_i ** (1._real64 / weight(i))
      if (key(i) < key(min_key_index)) min_key_index = i
    enddo

    u_i = random_real_in_range(a, b, seed(1))
    Xw = log(u_i) / log(key(min_key_index))

    ! Perform swaps at jump-intervals, until the population is exhausted
    i = m + 1
    do while(i <= n)
      Xw = Xw - weight(i)
      if (Xw <= 0._real64) then
        reservoir(min_key_index) = i
        thres_w = key(min_key_index)**weight(i)
        ! U__{i2} \in (t_w, 1)
        u_i2 = random_real_in_range(thres_w + a, b, seed(1))
        key(min_key_index) = u_i2 ** (1._real64 / weight(i))
        min_key_index = minloc(key, dim=1)
        u_i = random_real_in_range(a, b, seed(1))
        Xw = log(u_i) / log(key(min_key_index))
      endif
      i = i + 1
    enddo

    SAFE_DEALLOCATE_A(key)
    SAFE_DEALLOCATE_A(seed)

    POP_SUB(reservoir_sampling_aexpj)

  end subroutine reservoir_sampling_aexpj

end module quickrnd_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
