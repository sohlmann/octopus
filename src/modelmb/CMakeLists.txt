target_sources(Octopus_lib PRIVATE
		modelmb_1part.F90
		modelmb_density_matrix.F90
		modelmb_exchange_syms.F90
		modelmb_particles.F90
	        output_modelmb.F90
		young.F90
)
