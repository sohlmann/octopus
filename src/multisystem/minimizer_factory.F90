!! Copyright (C) 2024 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

!> @brief This module implements the factory for ground state algorithm
!!


module minimizer_factory_oct_m
  use, intrinsic :: iso_fortran_env
  use algorithm_factory_oct_m
  use algorithm_oct_m
  use debug_oct_m
  use electrons_oct_m
  use global_oct_m
  use minimizer_algorithm_oct_m
  use minimizer_static_oct_m
  use minimizer_scf_oct_m
  use interaction_partner_oct_m
  use messages_oct_m
  use multisystem_basic_oct_m
  use namespace_oct_m
  use parser_oct_m
  use unit_system_oct_m
  use varinfo_oct_m
  use system_oct_m
  implicit none

  private
  public :: minimizer_factory_t

  ! Known multisystem minimizers
  integer, public, parameter ::        &
    GROUND_STATE_STATIC           = 0, &
    GROUND_STATE_SCF              = 1


  !> @brief This class defines the factory for minimizers
  type, extends(algorithm_factory_t) :: minimizer_factory_t
    private
    integer :: max_iter !< Maximum number of iteration
  contains
    procedure :: create => minimizer_factory_create               !< @copydoc minimizer_factory_create
    procedure :: create_static => minimizer_factory_create_static !< @copydoc minimizer_factory_create_static
  end type minimizer_factory_t

  interface minimizer_factory_t
    module procedure minimizer_factory_constructor
  end interface minimizer_factory_t


contains
  ! ---------------------------------------------------------------------------------------
  !> @brief Constructor for the minimizer factory
  !!
  !! reads MaximumIter from the input file
  !
  function minimizer_factory_constructor(namespace) result(factory)
    type(namespace_t), intent(in) :: namespace
    type(minimizer_factory_t) :: factory

    PUSH_SUB(minimizer_factory_constructor)

    ! Get the maximum number of iterations
    ! This variable is also defined (and properly documented) in scf/scf.F90.
    call parse_variable(namespace, 'MaximumIter', 200, factory%max_iter)

    POP_SUB(minimizer_factory_constructor)
  end function minimizer_factory_constructor

  ! ---------------------------------------------------------------------------------------
  !> @brief Create a ground-state algorithm
  !
  function minimizer_factory_create(this, system) result(algorithm)
    class(minimizer_factory_t),  intent(in) :: this  !< the factory
    class(interaction_partner_t), intent(in), target :: system    !< the system using the minimizer
    class(algorithm_t), pointer :: algorithm                      !< the newly created minimizer

    integer :: default, algo_type
    class(minimizer_algorithm_t), pointer :: algo

    PUSH_SUB(minimizer_factory_create)

    !%Variable GroundStateAlgorithm
    !%Type integer
    !%Section SCF
    !%Description
    !% A variable to set the algorithm for the ground state.
    !%Option static 0
    !% Do not change during the SCF iterations. Default for anything apart electrons.
    !%Option scf 1
    !% Self-consistent field. Default for electrons.
    !%End
    default = GROUND_STATE_STATIC
    select type(system)
    type is(electrons_t)
      default = GROUND_STATE_SCF
    end select
    call parse_variable(system%namespace, 'GroundStateAlgorithm',default, algo_type)
    if (.not. varinfo_valid_option('GroundStateAlgorithm', algo_type)) then
      call messages_input_error(system%namespace, 'GroundStateAlgorithm')
    end if
    call messages_print_with_emphasis(msg='System algorithm', namespace=system%namespace)
    call messages_print_var_option('GroundStateAlgorithm', algo_type, namespace=system%namespace)

    select case (algo_type)
    case (GROUND_STATE_STATIC)
      algo => minimizer_static_t()
    case (GROUND_STATE_SCF)
      algo => minimizer_scf_t()
    case default
      call messages_input_error(system%namespace, 'GroudStateAlgorithm')
    end select

    call messages_print_with_emphasis(namespace=system%namespace)

    algo%max_iter = this%max_iter

    select type (system)
    class is (system_t)
      algo%system => system
    class default
      ASSERT(.false.)
    end select

    algorithm => algo

    POP_SUB(minimizer_factory_create)
  end function minimizer_factory_create

  ! ---------------------------------------------------------------------------------------
  !> @brief Create a static minimizer
  !!
  !! A static minimizer is needed for consistency for systems which do not
  !! change during an SCF procedure. One special example is the multisystem container, which itself can be kept
  !! static, as its components are updated.
  !
  function minimizer_factory_create_static(this, system) result(algorithm)
    class(minimizer_factory_t),  intent(in)         :: this    !< the factory
    class(interaction_partner_t), intent(in), target :: system  !< the system using the algorithm
    class(algorithm_t), pointer :: algorithm                    !< the newly created algorithm

    class(minimizer_algorithm_t), pointer :: minimizer

    PUSH_SUB(minimizer_factory_create_static)

    minimizer => minimizer_static_t()
    minimizer%max_iter = this%max_iter

    select type (system)
    class is (system_t)
      minimizer%system => system
    class default
      ASSERT(.false.)
    end select

    algorithm => minimizer

    POP_SUB(minimizer_factory_create_static)
  end function minimizer_factory_create_static


end module minimizer_factory_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
