target_sources(Octopus_lib PRIVATE
                clock.F90
		propagator.F90
		propagator_aetrs.F90
		propagator_beeman.F90
		propagator_bomd.F90
		propagator_exp_mid.F90
		propagator_exp_mid_2step.F90
		propagator_exp_gauss1.F90
		propagator_exp_gauss2.F90
		propagator_factory.F90
		propagator_leapfrog.F90
		propagator_rk4.F90
		propagator_static.F90
		propagator_verlet.F90
		)
