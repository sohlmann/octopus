!! Copyright (C)  2024 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

!> @brief This module implements the basic minimizer framework.
!!
module minimizer_algorithm_oct_m
  use algorithm_oct_m
  use debug_oct_m
  use global_oct_m
  use iteration_counter_oct_m
  use messages_oct_m
  use namespace_oct_m
  use system_oct_m

  implicit none

  private
  public ::                       &
    minimizer_algorithm_t,     &
    minimizer_algorithm_finished

  !> @brief Abstract class implementing minimizers
  !!
  !! Minimizers are implemented as a state machine. This abstract class defines the steps which
  !! are independent of the actual minimization algorithm, and also independent of the system, to
  !! which is is applied.
  type, extends(algorithm_t), abstract :: minimizer_algorithm_t
    private
    class(system_t), pointer, public :: system              !< The system using this minimizer
    integer, public :: max_iter                             !< Maximum number of iterations for self-consistent minimizers
    logical, public :: converged = .false.
  contains
    ! Below are the list of operations that needs to be implemented
    procedure :: do_operation => minimizer_algorithm_do_operation       !< @copydoc minimizer_algorithm_do_operation
    procedure :: finished => minimizer_algorithm_finished               !< @copydoc minimizer_algorithm_finished
    procedure :: init_iteration_counters => minimizer_algorithm_init_iteration_counters         !< @copydoc minimizer_algorithm_init_iteration_counters
    procedure :: write_output_header => minimizer_algorithm_write_output_header !< @copydoc minimizer_algorithm_write_output_header
    procedure :: continues_after_finished => minimizer_continues_after_finished
  end type minimizer_algorithm_t

contains

  !> @brief Try to perform one operation of the algorithm. Return .true. if sucessful.
  !!
  logical function minimizer_algorithm_do_operation(this, operation) result(done)
    class(minimizer_algorithm_t),           intent(inout) :: this
    type(algorithmic_operation_t), intent(in)    :: operation

    ! Nothing done yet here
    done = .false.

  end function minimizer_algorithm_do_operation

  ! ---------------------------------------------------------
  !> @brief indicate whether a minimizer has reached the final time
  !!
  logical function minimizer_algorithm_finished(this) result(finished)
    class(minimizer_algorithm_t), intent(in) :: this

    type(iteration_counter_t) :: iter

    iter = this%system%iteration
    finished = iter%value() > this%max_iter

    finished = finished .or. this%converged
  end function minimizer_algorithm_finished

  ! ---------------------------------------------------------
  !> Initialize the minimizer and system clocks.
  !!
  !! Note that we initialize the system clock here because it requires knowledge
  !! of the time-step.
  subroutine minimizer_algorithm_init_iteration_counters(this)
    class(minimizer_algorithm_t), intent(inout) :: this

    this%iteration = iteration_counter_t()
    this%system%iteration = iteration_counter_t()

  end subroutine minimizer_algorithm_init_iteration_counters


  subroutine minimizer_algorithm_write_output_header(this)
    class(minimizer_algorithm_t), intent(in) :: this

    PUSH_SUB(minimizer_algorithm_write_output_header)

    call messages_print_with_emphasis(msg="Multi-system minimizer", namespace=this%system%namespace)
    call messages_print_with_emphasis(namespace=this%system%namespace)

    POP_SUB(minimizer_algorithm_write_output_header)
  end subroutine minimizer_algorithm_write_output_header


  ! The GS algorithms need to keep going even when they are finished,
  ! otherwise other systems would get stuck at the update coupling step
  logical function minimizer_continues_after_finished(this)
    class(minimizer_algorithm_t), intent(in) :: this

    PUSH_SUB(minimizer_continues_after_finished)

    minimizer_continues_after_finished = .true.

    POP_SUB(minimizer_continues_after_finished)
  end function minimizer_continues_after_finished

end module minimizer_algorithm_oct_m


!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
