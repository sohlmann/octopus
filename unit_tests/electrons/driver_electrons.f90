 !> @brief Exposes all unit tests associated with the electrons folder to fortuno cmd line app
module driver_electrons_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_electrons

contains

    function testsuite_electrons() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_electrons

end module driver_electrons_oct_m