 !> @brief Exposes all unit tests associated with the hamiltonian folder to fortuno cmd line app
module driver_hamiltonian_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_hamiltonian

contains

    function testsuite_hamiltonian() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_hamiltonian

end module driver_hamiltonian_oct_m