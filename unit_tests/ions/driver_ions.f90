 !> @brief Exposes all unit tests associated with the ions folder to fortuno cmd line app
module driver_ions_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_ions

contains

    function testsuite_ions() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_ions

end module driver_ions_oct_m