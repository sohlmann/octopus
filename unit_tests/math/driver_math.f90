 !> @brief Exposes all unit tests associated with the math folder to fortuno cmd line app
module driver_math_oct_m
    use fortuno_interface_m, only: test_list

    use testsuite_lalg_basic_oct_m
    use testsuite_lalg_adv_oct_m
    use testsuite_quickrnd_oct_m

    implicit none
    private
    public :: testsuite_math

contains

    function testsuite_math() result(tests)
        type(test_list) :: tests

        tests = test_list([&
            testsuite_lalg_basic(), &
            testsuite_lalg_adv(), &
            testsuite_quickrnd()&
        ])

    end function testsuite_math

end module driver_math_oct_m
