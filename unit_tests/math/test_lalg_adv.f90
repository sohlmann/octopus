module testsuite_lalg_adv_oct_m
    use, intrinsic :: iso_fortran_env
    use fortuno_interface_m

    use lalg_adv_oct_m
  
    implicit none
    private
    public :: testsuite_lalg_adv
  
contains

     !> @brief Returns a suite instance, wrapped as test_item
    function testsuite_lalg_adv() result(res)
      type(test_item), allocatable :: res
  
      res = suite("math/lalg_adv", test_list([&
          test_case("test_pseudo_inverse_real", test_pseudo_inverse_real), &
          test_case("test_lalg_pseudo_inverse_complex", test_lalg_pseudo_inverse_complex) &
        ]))
  
    end function testsuite_lalg_adv
  

    !> @brief Test two implementations of the Moore-Penrose pseudo-inverse.
    subroutine test_pseudo_inverse_real()
        real(real64), allocatable :: a(:, :), a_inv(:, :)
        real(real64), allocatable :: expected_inv(:, :)

        ! -----------------------------------
        ! Test with a small, square matrix
        ! -----------------------------------

        expected_inv = transpose(reshape(&
        [0.0_real64, 0.0_real64,   0.0_real64, &
         0.0_real64, 1.0_real64,   0.0_real64, &
         0.0_real64, 0.0_real64, 1000.0_real64], shape=[3,3]))

        ! Routine a) with a square matrix
        ! Element (1,1) is less than threshold
        a = transpose(reshape(&
            [1.e-5_real64, 0.0_real64,   0.0_real64, &
               0.0_real64, 1.0_real64,   0.0_real64, &
               0.0_real64, 0.0_real64, 1.e-3_real64], shape=[3,3]))

        ! Create a copy as `a` is mutated 
        a_inv = a
        call lalg_svd_inverse(3, 3, a_inv, threshold=1.e-4_real64)
        call check(all_close(a_inv, expected_inv), msg='lalg_svd_inverse with a square matrix')

        ! Routine b) with a square matrix

        a_inv = a
        call lalg_pseudo_inverse(a_inv, threshold=1.e-4_real64)      
        call check(all_close(a_inv, expected_inv), msg='lalg_pseudo_inverse with a square matrix')

        deallocate(a)
        deallocate(a_inv)
        deallocate(expected_inv)

        ! -----------------------------------
        ! Repeat with a non-square matrix
        ! -----------------------------------

        expected_inv = transpose(reshape(&
        [0.0_real64,  0.0_real64,    0.0_real64, &
         0.0_real64,  1.0_real64,    0.0_real64, &
         0.0_real64,  0.0_real64, 1000.0_real64,&
         0.0_real64,  0.0_real64,    0.0_real64], shape=[3,4]))

         a = transpose(reshape(&
         [1.e-5_real64, 0.0_real64,   0.0_real64, 0.0_real64, &
            0.0_real64, 1.0_real64,   0.0_real64, 0.0_real64, &
            0.0_real64, 0.0_real64, 1.e-3_real64, 0.0_real64], shape=[4,3]))
        call check(all(shape(a) == [3, 4]), msg='Constructor shape(a)')

        ! Inverse shape
        call check(size(a, 1) == size(expected_inv, 2))
        call check(size(a, 2) == size(expected_inv, 1))

        ! Routine a) With a non-square matrix
        a_inv = a
        call lalg_svd_inverse(3, 4, a_inv, threshold=1.e-4_real64)
        call check(all(shape(a_inv) == [3, 4]), msg='lalg_svd_inverse preserves the input shape of a')
        call check(all_close(transpose(a_inv), expected_inv), msg='lalg_svd_inverse returns a^{-1} in the transpose of a')

        ! Routine b) With a non-square matrix 
        a_inv = a
        call lalg_pseudo_inverse(a_inv, threshold=1.e-4_real64) 
        call check(all(shape(a_inv) == [4, 3]), msg='lalg_pseudo_inverse reshapes a to shape(a^{-1}) on return')
        call check(all_close(a_inv, expected_inv), msg='lalg_pseudo_inverse reshapes a and returns the inv')

    end subroutine test_pseudo_inverse_real


    subroutine test_lalg_pseudo_inverse_complex()
        complex(real64), allocatable :: a(:, :), a_inv(:, :), a_reconstructed(:, :)
        complex(real64), allocatable :: expected_inv(:, :)

        integer :: i, j

        ! -----------------------------------
        ! Test with a small, square matrix
        ! -----------------------------------

        expected_inv = transpose(reshape(&
            [cmplx(-0.37408759, -0.84124088), cmplx( 0.31934307,  0.81569343), cmplx(-0.25547445, -0.20255474), &
             cmplx( 0.22445255,  0.30474453), cmplx(-0.19160584, -0.08941606), cmplx( 0.15328467, -0.07846715), &
             cmplx( 0.02372263,  0.37773723), cmplx( 0.05291971, -0.54197080), cmplx( 0.10766423,  0.23357664)  ], shape=[3,3]))

        a = transpose(reshape(&
            [cmplx(1.0, 2.0), cmplx(2.0, -1.0), cmplx(3.0,  3.0), &
             cmplx(4.0, 0.0), cmplx(5.0, -2.0), cmplx(6.0,  1.0), &
             cmplx(7.0, 1.0), cmplx(8.0,  2.0), cmplx(9.0, -1.0) ], shape=[3,3]))

        a_inv = a

        call lalg_pseudo_inverse(a_inv, threshold=1.e-5_real64)      
        call check(all_close(a_inv, expected_inv), msg='lalg_pseudo_inverse with a square matrix')

        a_reconstructed = matmul(a, matmul(a_inv, a))
        call check(all_close(a_reconstructed, a), msg='Verify the pseudo-inverse property: A * A+ * A ~ A')

        deallocate(a)
        deallocate(expected_inv)
        deallocate(a_reconstructed)

        ! -----------------------------------
        ! Repeat with a non-square matrix
        ! -----------------------------------

        a = transpose(reshape(&
            [cmplx(1.0,  2.0), cmplx(2.0, -1.0), cmplx(3.0,  3.0), &
             cmplx(4.0,  0.0), cmplx(5.0, -2.0), cmplx(6.0,  1.0), &
             cmplx(7.0,  1.0), cmplx(8.0,  2.0), cmplx(9.0, -1.0), &
             cmplx(2.0, -3.0), cmplx(1.0,  4.0), cmplx(0.0, -2.0)], shape=[3, 4]))

        expected_inv = transpose(reshape(&
          [cmplx( 0.00892642,  0.09347481), cmplx( 0.10471103, 0.23362687), cmplx(-0.05562774,-0.19387902), cmplx( 0.06219624, 0.27676724), &
           cmplx(-0.02355517,  0.05295703), cmplx(-0.04489678, 0.07054521), cmplx( 0.08743564,-0.05476156), cmplx(-0.05685482,-0.08134835), &
           cmplx( 0.04376594, -0.12403157), cmplx( 0.02964246,-0.23444493), cmplx( 0.01910399, 0.18834512), cmplx( 0.02908907,-0.13798662)], shape=[4, 3]))

        call check(all(shape(a) == [4, 3]))
        call check(all(shape(expected_inv) == [3, 4]))

        a_inv = a
        call lalg_pseudo_inverse(a_inv, threshold=1.e-5_real64) 

        call check(all(shape(a_inv) == [3, 4]), msg='lalg_pseudo_inverse reshapes a to shape(a^{-1}) on return')
        call check(all_close(a_inv, expected_inv), msg='lalg_pseudo_inverse reshapes a and returns the inv')

        a_reconstructed = matmul(a, matmul(a_inv, a))
        call check(all_close(a_reconstructed, a), msg='Verify the pseudo-inverse property: A * A+ * A ~ A')

    end subroutine test_lalg_pseudo_inverse_complex

end module testsuite_lalg_adv_oct_m
