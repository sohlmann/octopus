 !> @brief Exposes all unit tests associated with the scf folder to fortuno cmd line app
module driver_scf_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_scf

contains

    function testsuite_scf() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_scf

end module driver_scf_oct_m