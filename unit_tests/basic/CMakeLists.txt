target_sources(Octopus_test_suite PRIVATE
		driver_basic.f90  # Drivers are not registered as test ITEMS
		test_parser.f90
		test_sort.f90
)
foreach (test IN ITEMS
		basic/parser
		basic/sort
)
	Octopus_add_fortuno_test(TARGET Octopus_test_suite
			TEST_NAME ${test}
	)
endforeach ()
