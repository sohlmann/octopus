#[==============================================================================================[
#                                 LIKWID compatibility wrapper                                  #
]==============================================================================================]

#[===[.md
# FindLIKWID

LIKWID compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET LIKWID::LIKWID)
	return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT FindLIKWID)
find_path(LIKWID_INCLUDE_DIR
	NAMES likwid.h
	PATHS $ENV{LIKWID_HOME}
	PATH_SUFFIXES include
	REQUIRED)
find_library(LIKWID_LIBRARY
	NAMES likwid
	PATHS $ENV{LIKWID_HOME}
	REQUIRED)
find_package_handle_standard_args(${CMAKE_FIND_PACKAGE_NAME}
	REQUIRED_VARS LIKWID_INCLUDE_DIR LIKWID_LIBRARY
)

if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
	set(${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS ${LIKWID_INCLUDE_DIR})
	set(${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES ${LIKWID_LIBRARY})
	add_library(LIKWID::LIKWID INTERFACE IMPORTED)
	target_include_directories(LIKWID::LIKWID INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS})
	target_link_libraries(LIKWID::LIKWID INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES})
	set(HAVE_LIKWID 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
	URL https://hpc.fau.de/research/tools/likwid/
	DESCRIPTION "Performance monitoring and benchmarking suite"
	PURPOSE "Performance analysis"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
