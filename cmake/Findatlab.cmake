#[==============================================================================================[
#                                  atlab compatibility wrapper                                  #
]==============================================================================================]

#[===[.md
# Findatlab

atlab compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET atlab::atlab)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT Findatlab)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        NAMES atlab
        PKG_MODULE_NAMES atlab)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(atlab::atlab ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://gitlab.com/l_sim/atlab
        DESCRIPTION "The atlab library handles the atomic positions and much more"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
