include_guard(GLOBAL)

function(perl_find_module)
	cmake_parse_arguments(PARSE_ARGV 0 _pfm "REQUIRED" "NAME;RESULT_VARIABLE" "")
	execute_process(
		COMMAND "${PERL_EXECUTABLE}" "-M${_pfm_NAME}" -e 1
		RESULT_VARIABLE status)
	if(status EQUAL 0)
		set("${_pfm_RESULT_VARIABLE}" 1 PARENT_SCOPE)
	else()
		set("${_pfm_RESULT_VARIABLE}" "${_pfm_NAME}-NOTFOUND" PARENT_SCOPE)
	endif()
	if (_pfm_REQUIRED AND NOT status EQUAL 0)
		message(FATAL_ERROR "Perl module ${_pfm_NAME} could not be found")
	endif()
endfunction()
