#[==============================================================================================[
#                                libvdwxc compatibility wrapper                                #
]==============================================================================================]

#[===[.md
# Findlibvdwxc

libvdwxc compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET libvdwxc::libvdwxc)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT Findlibvdwxc)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        NAMES libvdwxc
        PKG_MODULE_NAMES libvdwxc)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(libvdwxc::libvdwxc ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
    # libvdwxc does not provide a pkg-config for the fortran bindings. Assuming they are installed in the same place
    # and adding it manually if not found
    get_target_property(_link_libraries PkgConfig::${CMAKE_FIND_PACKAGE_NAME} INTERFACE_LINK_LIBRARIES)
    if (NOT vdwxcfort IN_LIST _link_libraries)
        find_library(vdwxcfort_LIBRARY
                NAMES vdwxcfort
        )
        mark_as_advanced(vdwxcfort_LIBRARY)
        if (NOT vdwxcfort_LIBRARY)
            set(${CMAKE_FIND_PACKAGE_NAME}_FOUND FALSE)
            return()
        endif ()
        target_link_libraries(PkgConfig::${CMAKE_FIND_PACKAGE_NAME} INTERFACE ${vdwxcfort_LIBRARY})
    endif ()
    # Fortran (pkg-config) package does not expose if the C library was compiled with mpi or not, leading to unlinked
    # references between the Fortran and C library. Set `HAVE_LIBVDWXC_MPI` according to vdwxc_has_mpi
    if (CMAKE_VERSION LESS 3.25)
        try_run(libvdwxc_has_mpi_run libvdwxc_has_mpi_compile ${CMAKE_CURRENT_BINARY_DIR}/CMakeFiles/CMakeTmp
                SOURCES ${CMAKE_CURRENT_LIST_DIR}/test_libvdwxc_mpi.c
                LINK_LIBRARIES PkgConfig::${CMAKE_FIND_PACKAGE_NAME}
                RUN_OUTPUT_VARIABLE libvdwxc_has_mpi
        )
    else ()
        # TODO: Move to SOURCE_FROM_VAR when moving to 3.25
        try_run(libvdwxc_has_mpi_run libvdwxc_has_mpi_compile
                SOURCES ${CMAKE_CURRENT_LIST_DIR}/test_libvdwxc_mpi.c
                LINK_LIBRARIES PkgConfig::${CMAKE_FIND_PACKAGE_NAME}
                RUN_OUTPUT_STDOUT_VARIABLE libvdwxc_has_mpi
                NO_CACHE
        )
    endif ()
    if (NOT libvdwxc_has_mpi_compile OR libvdwxc_has_mpi_run STREQUAL FAILED_TO_RUN OR
            NOT libvdwxc_has_mpi_run EQUAL 0)
        # Check if the test program managed to compile. If not, there is something wrong with the api probably
        message(WARNING "Failed to compile test_libvdwxc_mpi.c")
        set(${CMAKE_FIND_PACKAGE_NAME}_FOUND FALSE)
        # TODO: This can be simplified with cmake_language(DEFER) with CMake 3.19
        list(POP_BACK CMAKE_MESSAGE_CONTEXT)
        return()
    endif ()
    if (libvdwxc_has_mpi)
        set(HAVE_LIBVDWXC_MPI 1)
    endif ()
endif ()
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    set(HAVE_LIBVDWXC 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://gitlab.com/libvdwxc/libvdwxc
        DESCRIPTION "Portable C library of density functionals with van der Waals interactions for density functional theory"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
