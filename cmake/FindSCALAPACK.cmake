#[==============================================================================================[
#                                ScaLAPACK compatibility wrapper                                #
]==============================================================================================]

#[===[.md
# FindSCALAPACK

ScaLAPACK compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET SCALAPACK::scalapack)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT FindSCALAPACK)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        NAMES SCALAPACK ScaLAPACK scalapack
        PKG_MODULE_NAMES scalapack)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(SCALAPACK::scalapack ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
elseif (NOT TARGET SCALAPACK::scalapack AND TARGET scalapack)
    # Upstream does not use namespace
    add_library(SCALAPACK::scalapack ALIAS scalapack)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://www.netlib.org/scalapack/
        DESCRIPTION "ScaLAPACK is a library of high-performance linear algebra routines for parallel distributed memory machines"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
