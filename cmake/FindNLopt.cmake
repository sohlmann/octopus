#[==============================================================================================[
#                                  NLopt compatibility wrapper                                  #
]==============================================================================================]

#[===[.md
# FindNLopt

NLopt compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET NLopt::nlopt)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT FindNLopt)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        NAMES NLopt nlopt
        PKG_MODULE_NAMES nlopt)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(NLopt::nlopt ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    set(HAVE_NLOPT 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://github.com/stevengj/nlopt
        DESCRIPTION "Library for nonlinear optimization, wrapping many algorithms for global and local, constrained or unconstrained, optimization "
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
