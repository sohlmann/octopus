#[==============================================================================================[
#                                  METIS compatibility wrapper                                  #
]==============================================================================================]

#[===[.md
# FindMETIS

METIS compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET METIS::metis)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT FindMETIS)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        HAVE_FALLBACK
        NAMES METIS metis
        PKG_MODULE_NAMES metis)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(METIS::metis ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()
# Temporary workarounds for non-packages
if (NOT ${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    find_library(METIS_LIBRARY
            NAMES metis
    )
    mark_as_advanced(METIS_LIBRARY)
    find_path(METIS_INCLUDE_DIR
            NAMES metis.h
            PATH_SUFFIXES metis
    )
    mark_as_advanced(METIS_INCLUDE_DIR)
    find_package_handle_standard_args(${CMAKE_FIND_PACKAGE_NAME}
            REQUIRED_VARS METIS_LIBRARY METIS_INCLUDE_DIR
    )
    if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
        set(${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS ${METIS_INCLUDE_DIR})
        set(${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES ${METIS_LIBRARY})
        add_library(METIS::metis UNKNOWN IMPORTED)
        set_target_properties(METIS::metis PROPERTIES
                IMPORTED_LOCATION ${METIS_LIBRARY}
                IMPORTED_LINK_INTERFACE_LANGUAGES C
        )
        target_include_directories(METIS::metis INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS})
        target_link_libraries(METIS::metis INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES})
    endif()
endif ()
if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    set(HAVE_METIS 1)
    # Bug in LINK_LIBRARIES and aliased target. Need to get the real target
    # https://gitlab.kitware.com/cmake/cmake/-/issues/25337
    get_target_property(_real_target METIS::metis ALIASED_TARGET)
    if (NOT _real_target)
        set(_real_target METIS::metis)
    endif ()
    if (CMAKE_VERSION LESS 3.25)
        try_compile(IDXTYPEWIDTH_64 ${CMAKE_CURRENT_BINARY_DIR}/CMakeFiles/CMakeTmp
                SOURCES ${CMAKE_CURRENT_LIST_DIR}/test_metis_idxtypewidth.c
                LINK_LIBRARIES ${_real_target}
                NO_CACHE
        )
    else ()
        try_compile(IDXTYPEWIDTH_64
                SOURCES ${CMAKE_CURRENT_LIST_DIR}/test_metis_idxtypewidth.c
                LINK_LIBRARIES ${_real_target}
                NO_CACHE
        )
    endif ()
    if (IDXTYPEWIDTH_64)
        set(METIS_IDXTYPEWIDTH 64)
    else ()
        set(METIS_IDXTYPEWIDTH 32)
    endif ()
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://github.com/KarypisLab/METIS
        DESCRIPTION "METIS - Serial Graph Partitioning and Fill-reducing Matrix Ordering "
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
