# Octopus

Octopus is a scientific program allowing to describe non-equilibrium phenomena
in molecular complexes, low dimensional materials, and extended systems. In
usual applications, electrons are described quantum-mechanically within
density-functional theory (DFT), or in its time-dependent form (TDDFT) when
doing simulations in time, using a real-space grid, while nuclei are described
classically as point particles. Electromagnetic fields can be treated either
classically or quantum mechanically within a generalized time-dependent density
functional theory.

For optimal execution performance Octopus is parallelized using MPI and OpenMP
and can scale to tens of thousands of processors.  It also has support for
graphical processing units (GPUs) through CUDA and OpenCL.

<!-- TOC -->

- [Octopus](#octopus)
  - [How to get Octopus](#how-to-get-octopus)
  - [Requirements](#requirements)
    - [Build from git repository](#build-from-git-repository)
      - [Autotools](#autotools)
      - [CMake](#cmake)
    - [Build from tarball archive](#build-from-tarball-archive)
  - [Running Octopus](#running-octopus)
  - [Test Suite](#test-suite)
  - [Contributing](#contributing)
  - [Supported Integrations](#supported-integrations)
  - [External Dependencies](#external-dependencies)
  - [License](#license)
  - [How to cite Octopus](#how-to-cite-octopus)

<!-- TOC -->

## How to get Octopus

Octopus can either be built from source or installed using some packaging
format. Building from source can be done either from [tarball
archives](#build-from-tarball-archive) or directly from the [git
repository](#build-from-git-repository). For the latest releases, supported
packaging formats that we are aware of include [Spack](https://spack.io/) and
[MacPorts](https://ports.macports.org/port/octopus/).

In the case of running Octopus in an HPC environment where performance is
critical, we recommended using an installation of the code managed by the HPC
administrators. In case a different version is needed than the one available, we
recommend getting in touch with the HPC administrators.

## Requirements

* Supported Fortran compilers:
  * [gfortran](https://gcc.gnu.org/releases.html) 10 - 14
  * ifort 2021 - 2022, [ifx](https://www.intel.com/content/www/us/en/developer/tools/oneapi/fortran-compiler.html) 2024.0.0
    * Although ifx can be used to compile Octopus, it results in numerous test suite failures, and should not be considered reliable for production calculations
  
* Supported C and C++ compilers:
  * Any C11-compliant C compiler, and any C++17-compliant compiler, respectively

* MPI: 
  * [openmpi](https://www.open-mpi.org/software/ompi/v5.0/) >= 4.0.7
  * [MPICH](https://github.com/pmodels/mpich/releases/tag/v4.2.0) >= 4.2.0

* Build System:
  * autotools (autoconf 2.64)
  * [CMake](https://cmake.org) >= 3.20 and [Ninja](https://ninja-build.org)

* Optional GPU Support
  * CUDA
  * HIP
  * OpenCL

### Build from git repository

The latest development branch of Octopus can be cloned directly from [Gitlab](https://gitlab.com/octopus-code/octopus):

```console
$ git clone https://gitlab.com/octopus-code/octopus.git
```

providing developers with the latest features and fixes. Changes are continuously merged into the `main` branch, with stable releases versioned using git tags. To see the latest tagged versions of Octopus, and checkout: 

```console
$ git tag --sort=-creatordate
$ git checkout 15.1
```

One can also directly clone tagged release versions with:

```
$ git clone --branch 15.1 --single-branch https://gitlab.com/octopus-code/octopus.git
```

which checks out the tag in a detached HEAD state.

#### Autotools

To build Octopus directly from the git repository, you need to generate the build files, run configure, and make:

```console
# Generate build files - autoreconf not required for tarball release
$ autoreconf --install
$ ./configure
$ make
$ make install
```

For more configuration details see the [installation page](https://Octopus-code.org/documentation/main/manual/install/).

#### CMake

Octopus is transitioning to the CMake build system, and is almost feature-complete. 
A minimal, parallel build of Octopus can be configured and built with:

```console
$ cmake -B build-min-mpi -G Ninja --fresh -DOCTOPUS_MPI=On -DOCTOPUS_OpenMP=On -DCMAKE_DISABLE_FIND_PACKAGE_Libxc=On -DCMAKE_DISABLE_FIND_PACKAGE_SPGLIB=On -DCMAKE_DISABLE_FIND_PACKAGE_METIS=On
$ cmake --build build-min-mpi -j
```

where the Libxc and SPGLIB are obtained with CMake's FetchContent and built from source, and the internally-packaged version of Metis 5.1 is used. Note that [Ninja](https://ninja-build.org) is a hard requirement when compiling with gfortran/GCC due to how Octopus's macros are preprocessed.

If you have multiple compilers or library versions installed on your system, you may need to 
explicitly specify which versions you want CMake to use. See the CMake [README](cmake/README.md) for more details.

### Build from tarball archive

Versioned tarball archives that can be downloaded from the
[official website](https://Octopus-code.org/documentation/main/releases/), with separate packages available for building with CMake and autotools, respectively. Note, there is no git repository information in the tarball releases, so `git submodule` cannot be used.

## Running Octopus

The main interface of Octopus are the `inp` files. Such a file needs to be
present in the work directory for Octopus to run, after which simply run
`octopus`:

```console
$ cat inp
CalculationMode = gs
PeriodicDimensions = 3
a = 10.2
BoxShape = parallelepiped
%LatticeParameters
  a | a | a
%
%LatticeVectors
 0.  | 0.5 | 0.5
 0.5 | 0.  | 0.5
 0.5 | 0.5 | 0.0
%
%ReducedCoordinates
 "Si" |   0.0    | 0.0     | 0.0
 "Si" |   1/4    | 1/4     | 1/4
%
Spacing = 0.5
%KPointsGrid
 4 | 4 | 4
%
$ Octopus
```

## Test Suite

Octopus is packaged with its own application test framework and test suite. For details on how to run the tests
following successful installation, please consult `testsuite/README`.

## Contributing

This project is primarily developed on gitlab.com, at [Octopus-code/Octopus](https://gitlab.com/Octopus-code/Octopus).
Issues and merge requests are welcome. See [Contributing.md](Contributing.md) for general guidelines for contributions
and [Development.md](Development.md) for recommendations on how to setup your development environment.

## Supported Integrations

- MPI and OpenMP
- GPU computation via OpenCL, CUDA and HIP
- [Postopus](https://gitlab.com/Octopus-code/postopus)

## External Dependencies

Required External Packages (all builds):
* [Libxc](https://libxc.gitlab.io/)
* [Spglib](https://spglib.readthedocs.io/en/latest/)
* [FFTW](https://www.fftw.org) or [MKL]
* [BLAS and LAPACK](https://www.netlib.org/) or [MKL]
* [GSL](https://www.gnu.org/software/gsl/)

[MKL]: https://www.intel.com/content/www/us/en/developer/tools/oneapi/onemkl.html

Required External Packages (MPI):
* [METIS](https://github.com/KarypisLab/METIS)

Packages Absorbed in `external_libs`:
* bpdn
* dftd3
* fortrancl
* metis-5.1
* qshep
* rapidxml
* spglib-2.1.0 (depreciated)

Optional External Packages:
* [BerkleyGW](https://berkeleygw.org)
* [CGAL](https://www.cgal.org)
* [clfft](https://github.com/clMathLibraries/clFFT)
* [DftbPlus](https://github.com/dftbplus/dftbplus)
* [ELPA](https://github.com/marekandreas/elpa/tree/master) (depends on SCALAPACK for optimal usage)
* [etsf-io](https://github.com/ElectronicStructureLibrary/libetsf_io)
* [GD](https://libgd.github.io)
* [libvdwxc](https://libvdwxc.gitlab.io/libvdwxc/) (depends on FFTW)
* [netCDF](https://github.com/Unidata/netcdf-fortran) (Fortran)
* [nfft](https://github.com/NFFT/nfft)
* [NLopt](https://github.com/stevengj/nlopt)
* [ParMETIS](https://github.com/KarypisLab/ParMETIS) (depends on Metis)
* [pfft](https://github.com/mpip/pfft) (depends on FFTW-MPI)
* [pnfft](https://github.com/mpip/pnfft) (depends on FFTW-MPI and pnfft)
* [PSolver](https://github.com/NNairIITK/BigDFT) with [atlab](https://github.com/NNairIITK/BigDFT)
* [pspio](https://gitlab.com/ElectronicStructureLibrary/libpspio/-/releases)
* [SCALAPACK](https://www.netlib.org/scalapack/)
* [SPARSKIT](https://github.com/efocht/SPARSKIT2)

External dependencies are also installable as part of [spack configurations](https://gitlab.gwdg.de/mpsd-cs/spack-environments/)
for specific FOSS and Intel toolchains. 

Note, if using MKL, FFTW-MPI is not available. This is because MKL does not [ship prebuilt](https://www.intel.com/content/www/us/en/docs/onemkl/developer-reference-fortran/2024-1/building-your-own-wrapper-library.html)] API for FFTW-MPI.

## License

Octopus is free software, distributed under the terms of GNU General Public
License version 3. You are free to download it and use it. The individual source
files that make up Octopus are licensed either under the GPL version 2 or higher
or under the Mozilla Public License version 2. You can freely modify the source
files granted you follow the corresponding license terms and respect the
attributions.

Octopus also includes bundled code with different compatible license. Please see
[`COPYING`](COPYING) for more details.

## How to cite Octopus

Octopus is a free program, so you have the right to use, change, distribute, and
to publish papers with it without citing anyone (for as long as you follow the
GPL license). However, its considered a good scientific practice to cite one or
more paper concerning Octopus in an article that uses it. We recommend citing at
least the latest paper describing the code:

N. Tancogne-Dejean, M. J. T. Oliveira, X. Andrade, H. Appel, C. H. Borca, G. Le
Breton, F. Buchholz, A. Castro, S. Corni, A. A. Correa, U. De Giovannini,
A. Delgado, F. G. Eich, J. Flick, G. Gil, A. Gomez, N. Helbig, H. Hübener,
R. Jestädt, J. Jornet-Somoza, A. H. Larsen, I. V. Lebedeva, M. Lüders,
M. A. L. Marques, S. T. Ohlmann, S. Pipolo, M. Rampp, C. A. Rozzi,
D. A. Strubbe, S. A. Sato, C. Schäfer, I. Theophilou, A. Welden, A. Rubio,
_"Octopus, a computational framework for exploring light-driven phenomena and
quantum dynamics in extended and finite systems"_, [The Journal of Chemical
Physics](https://doi.org/10.1063/1.5142502) **152** 124119 (2020).

One can find a more complete list of papers describing Octopus and some of its
features on the [official
website](https://octopus-code.org/documentation/main/citing_octopus/).
