.foss-compiler-mpi:
  - export CC="mpicc"
  - export CXX="mpicxx"
  - export FC="mpif90"
  - export MPIEXEC="orterun --map-by socket --oversubscribe"

.foss-compiler-flags-standard:
  - export CFLAGS="-O2 -march=native -ftest-coverage -fprofile-arcs -fprofile-update=single"
  - export CXXFLAGS="-O2 -march=native -ftest-coverage -fprofile-arcs -fprofile-update=single"
  - export FCFLAGS="-O2 -march=native -fbacktrace -ftest-coverage -fprofile-arcs -fprofile-update=single -ffree-line-length-none"

.foss-compiler-flags-debug:
  - export CFLAGS="-g -Wall -O2 -march=native -Wextra -pedantic -ftest-coverage -fprofile-arcs -fprofile-update=single"
  - export CXXFLAGS="-g -Wall -O2 -march=native -Wextra -pedantic -ftest-coverage -fprofile-arcs -fprofile-update=single"
  - export FCFLAGS="-g -fno-var-tracking-assignments -Wall -Wno-maybe-uninitialized -Wno-surprising -O2 -march=native -fbacktrace -fcheck=all -fbounds-check -finit-real=snan -finit-logical=false -finit-derived -ffpe-trap=zero,invalid -ftest-coverage -fprofile-arcs -fprofile-update=single -ffree-line-length-none"

.foss-compiler-flags-opt:
  - export CFLAGS="-O3 -march=native -funroll-loops"
  - export CXXFLAGS="-O2 -march=native"
  - export FCFLAGS="-O3 -march=native -fbacktrace -funroll-loops -ffree-line-length-none"

.foss-compiler-flags-warnings:
  - export CFLAGS="-O0 -Wall -Wno-unknown-pragmas"
  - export CXXFLAGS="-O0 -Wall -Wno-int-to-pointer-cast -Wno-switch -Wno-sign-compare -Wno-return-type -Wno-catch-value"
  - export FCFLAGS="-O0 -Wall -Wno-maybe-uninitialized -Wno-unused-dummy-argument -Wno-c-binding-type -Wno-surprising"

.foss-compiler-flags-dist:
  - export CFLAGS="-O0"
  - export CXXFLAGS="-O0"
  - export FCFLAGS="-O0 -DSHARE_DIR=/tmp -ffree-line-length-none"

.foss-compiler-flags-valgrind:
  - export CFLAGS="-g -O2 -march=native"
  - export CXXFLAGS="-g -O2 -march=native"
  - export FCFLAGS="-g -fno-var-tracking-assignments -O2 -march=native -fbacktrace -ffree-line-length-none"

.foss-compiler-flags-ppc:
  - export CFLAGS="-O2 -mcpu=native"
  - export CXXFLAGS="-O2 -mcpu=native"
  - export FCFLAGS="-O2 -mcpu=native -fbacktrace -fcheck=all -fbounds-check -ffree-line-length-none"

.foss_configure_flags_serial_min:
  variables:
    CONFIGURE_FLAGS:
      value: '--enable-silent-deprecation
        --with-libxc-prefix="${MPSD_LIBXC_ROOT}"
        --with-gsl-prefix="${MPSD_GSL_ROOT}"
        --with-blas="-L${MPSD_OPENBLAS_ROOT}/lib -lopenblas"
        --with-fftw-prefix="${MPSD_FFTW_ROOT}"'
      expand: false

.foss_configure_flags_serial:
  variables:
    CONFIGURE_FLAGS:
      value: '--enable-silent-deprecation
        --with-libxc-prefix="${MPSD_LIBXC_ROOT}"
        --with-gsl-prefix="${MPSD_GSL_ROOT}"
        --with-netcdf-prefix="${MPSD_NETCDF_FORTRAN_ROOT}"
        --with-etsf-io-prefix="${MPSD_ETSF_IO_ROOT}"
        --with-sparskit="-L${MPSD_SPARSKIT_ROOT}/lib -lskit"
        --with-nlopt-prefix="${MPSD_NLOPT_ROOT}"
        --with-dftbplus-prefix="${MPSD_DFTBPLUS_ROOT}"
        --with-atlab-prefix="${MPSD_BIGDFT_ATLAB_ROOT}"
        --with-psolver-prefix="${MPSD_BIGDFT_PSOLVER_ROOT}"
        --with-futile-prefix="${MPSD_BIGDFT_FUTILE_ROOT}"
        --with-pspio-prefix="${MPSD_LIBPSPIO_ROOT}"
        --with-fftw-prefix="${MPSD_FFTW_ROOT}"
        --with-nfft="${MPSD_NFFT_ROOT}"
        --with-libvdwxc-prefix="${MPSD_LIBVDWXC_ROOT}"
        --with-blas="-L${MPSD_OPENBLAS_ROOT}/lib -lopenblas"'
        # missing: poke
        # --with-berkeleygw-prefix="${MPSD_BERKELEYGW_ROOT}"
      expand: false

.foss_configure_flags_valgrind:
  variables:
    CONFIGURE_FLAGS:
      value: '--enable-silent-deprecation
        --with-libxc-prefix="${MPSD_LIBXC_ROOT}"
        --with-gsl-prefix="${MPSD_GSL_ROOT}"
        --with-netcdf-prefix="${MPSD_NETCDF_FORTRAN_ROOT}"
        --with-etsf-io-prefix="${MPSD_ETSF_IO_ROOT}"
        --with-sparskit="-L${MPSD_SPARSKIT_ROOT}/lib -lskit"
        --with-nlopt-prefix="${MPSD_NLOPT_ROOT}"
        --with-atlab-prefix="${MPSD_BIGDFT_ATLAB_ROOT}"
        --with-psolver-prefix="${MPSD_BIGDFT_PSOLVER_ROOT}"
        --with-futile-prefix="${MPSD_BIGDFT_FUTILE_ROOT}"
        --with-pspio-prefix="${MPSD_LIBPSPIO_ROOT}"
        --with-fftw-prefix="${MPSD_FFTW_ROOT}"
        --with-nfft="${MPSD_NFFT_ROOT}"
        --with-libvdwxc-prefix="${MPSD_LIBVDWXC_ROOT}"
        --with-blas="-L${MPSD_OPENBLAS_ROOT}/lib -lopenblas"
        --without-dftbplus
        --without-cgal'
        # missing: poke
        # --with-berkeleygw-prefix="${MPSD_BERKELEYGW_ROOT}"
      expand: false

.foss_configure_flags_ppc:
  variables:
    CONFIGURE_FLAGS:
      value: '--enable-silent-deprecation
        --with-libxc-prefix="${MPSD_LIBXC_ROOT}"
        --with-gsl-prefix="${MPSD_GSL_ROOT}"
        --with-netcdf-prefix="${MPSD_NETCDF_FORTRAN_ROOT}"
        --with-etsf-io-prefix="${MPSD_ETSF_IO_ROOT}"
        --with-sparskit="-L${MPSD_SPARSKIT_ROOT}/lib -lskit"
        --with-nlopt-prefix="${MPSD_NLOPT_ROOT}"
        --with-dftbplus-prefix="${MPSD_DFTBPLUS_ROOT}"
        --with-atlab-prefix="${MPSD_BIGDFT_ATLAB_ROOT}"
        --with-psolver-prefix="${MPSD_BIGDFT_PSOLVER_ROOT}"
        --with-futile-prefix="${MPSD_BIGDFT_FUTILE_ROOT}"
        --with-pspio-prefix="${MPSD_LIBPSPIO_ROOT}"
        --with-fftw-prefix="${MPSD_FFTW_ROOT}"
        --with-nfft="${MPSD_NFFT_ROOT}"
        --with-libvdwxc-prefix="${MPSD_LIBVDWXC_ROOT}"
        --with-blas="-L${MPSD_OPENBLAS_ROOT}/lib -lopenblas -lpthread"
        --without-cgal'
        # missing: poke
      expand: false

.foss_configure_flags_mpi_min:
  variables:
    MPI_CONFIGURE_FLAGS:
      value: '--enable-mpi'
      expand: false

.foss_configure_flags_mpi:
  variables:
    MPI_CONFIGURE_FLAGS:
      value: '--enable-mpi
        --with-elpa-prefix="${MPSD_ELPA_ROOT}"
        --with-pfft-prefix="${MPSD_PFFT_ROOT}"
        --with-pnfft-prefix="${MPSD_PNFFT_ROOT}"
        --with-blacs="-L${MPSD_NETLIB_SCALAPACK_ROOT}/lib -lscalapack"'
      expand: false






.intel-compiler-serial:
  - export FC="ifort"
  - export CC="icc"
  - export CXX="icpc"

.intel-compiler-mpi:
  - export FC="mpiifort"
  - export CC="mpiicc"
  - export CXX="mpiicpc"
  - export MPIEXEC="mpirun --map-by socket"
  # prevent IMPI from using slurm to create new MPI ranks
  - export I_MPI_HYDRA_BOOTSTRAP=fork

.intel-compiler-flags:
  - export CFLAGS="-O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback"
  - export CXXFLAGS="-O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback"
  - export FCFLAGS="-O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback"

.intel-compiler-flags-warnings:
  - export CFLAGS="-O0 -Wall -Wno-unknown-pragmas -diag-disable:167,10441"
  - export CXXFLAGS="-O0 -Wall -Wno-int-to-pointer-cast -Wno-switch -Wno-sign-compare -Wno-return-type -diag-disable=10441"
  - export FCFLAGS="-O0 -warn all,noexternals,nouninitialized,nounused -diag-disable:5462,6843"

.intel_configure_flags_serial_min:
  variables:
    CONFIGURE_FLAGS:
      value: '--enable-silent-deprecation
        --with-libxc-prefix="${MPSD_LIBXC_ROOT}"
        --with-gsl-prefix="${MPSD_GSL_ROOT}"
        --with-blas="-L${MPSD_INTEL_ONEAPI_MKL_ROOT}/lib/intel64 -Wl,-Bstatic -Wl,--start-group -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -Wl,--end-group -Wl,-Bdynamic"'
      expand: false

.intel_configure_flags_serial:
  variables:
    CONFIGURE_FLAGS:
      value: '--enable-silent-deprecation
        --with-libxc-prefix="${MPSD_LIBXC_ROOT}"
        --with-gsl-prefix="${MPSD_GSL_ROOT}"
        --with-netcdf-prefix="${MPSD_NETCDF_FORTRAN_ROOT}"
        --with-etsf-io-prefix="${MPSD_ETSF_IO_ROOT}"
        --with-sparskit="-L${MPSD_SPARSKIT_ROOT}/lib -lskit"
        --with-nlopt-prefix="${MPSD_NLOPT_ROOT}"
        --with-dftbplus-prefix="${MPSD_DFTBPLUS_ROOT}"
        --with-psolver-prefix="${MPSD_BIGDFT_PSOLVER_ROOT}"
        --with-futile-prefix="${MPSD_BIGDFT_FUTILE_ROOT}"
        --with-pspio-prefix="${MPSD_LIBPSPIO_ROOT}"
        --with-nfft="${MPSD_NFFT_ROOT}"
        --disable-gdlib'
      expand: false
    WITH_BLAS:
      value: '--with-blas="-L${MPSD_INTEL_ONEAPI_MKL_ROOT}/lib/intel64 -Wl,-Bstatic -Wl,--start-group -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -Wl,--end-group -Wl,-Bdynamic"'
      expand: false
    WITH_BLAS_OPENMP:
      value: '--with-blas="-L${MPSD_INTEL_ONEAPI_MKL_ROOT}/lib/intel64 -Wl,-Bstatic -Wl,--start-group -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -Wl,--end-group -Wl,-Bdynamic"'
      expand: false

.intel_configure_flags_mpi:
  variables:
    MPI_CONFIGURE_FLAGS:
      value: '--enable-mpi --enable-mpi_mod
        --with-elpa-prefix="${MPSD_ELPA_ROOT}"'
      expand: false
    WITH_BLACS:
      value: '--with-blacs="-L${MPSD_INTEL_ONEAPI_MKL_ROOT}/lib/intel64 -Wl,-Bstatic -Wl,--start-group -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64 -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -Wl,--end-group -Wl,-Bdynamic"'
      expand: false
    WITH_BLACS_OPENMP:
      value: '--with-blacs="-L${MPSD_INTEL_ONEAPI_MKL_ROOT}/lib/intel64 -Wl,-Bstatic -Wl,--start-group -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -Wl,--end-group -Wl,-Bdynamic"'
      expand: false

