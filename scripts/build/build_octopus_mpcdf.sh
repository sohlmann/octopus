#!/usr/bin/env bash
# shellcheck disable=SC2154

set -euo pipefail

_script_src=$(readlink -f -- "${BASH_SOURCE[0]}")
_script_dir=$(dirname -- "${_script_src}")

_arg_config="on"
_arg_install="on"
_arg_prefix="${PWD}/install"
_arg_builddir="${PWD}/_build"
_arg_parallel=6
_arg_verbose=""
_arg_debug_asan="off"
_arg_debug_full="off"

_build_type="Release"

die()
{
    _die_ret="${2:-1}"
    test "${_PRINT_HELP:-no}" = yes && print_help >&2
    echo "$1" >&2
    exit "${_die_ret}"
}

print_help()
{
    printf '%s\n' "Compilation script for octopus using cmake."
    print_usage
}

print_usage()
{
    printf '\nUsage: %s [OPTIONS]\n' "$0"
    printf '\t%s\n' "--no-config: skip configure step, useful for recompiling (will be run by default)"
    printf '\t%s\n' "--no-install: skip install step (will be run by default)"
    printf '\t%s\n' "--debug-asan: debug build with address sanitizer (off by default)"
    printf '\t%s\n' "--debug-full: debug build with bounds checking and fpe trapping (off by default)"
    printf '\t%s\n' "--parallel N: build on N threads in parallel (default: 6)"
    printf '\t%s\n' "--prefix DIR: install into DIR (default: ${PWD}/install)"
    printf '\t%s\n' "--builddir DIR: build in DIR (default: ${PWD}/_build)"
    printf '\t%s\n' "--destdir DIR: install using DESTDIR=DIR (default: no)"
    printf '\t%s\n' "--verbose: call cmake with --verbose (default: no)"
    printf '\t%s\n' "-h, --help: print help"
}

parse_commandline()
{
    while test $# -gt 0
    do
        _key="$1"
        case "${_key}" in
            --no-config|--config)
                test "${1:0:5}" = "--no-" && _arg_config="off"
                ;;
            --no-install|--install)
                test "${1:0:5}" = "--no-" && _arg_install="off"
                ;;
            --debug-asan)
                _arg_debug_asan="on"
                # custom build type to avoid additional flags by cmake
                _build_type="mpcdf"
                ;;
            --debug-full)
                _arg_debug_full="on"
                # custom build type to avoid additional flags by cmake
                _build_type="mpcdf"
                ;;
            --parallel)
                test $# -lt 2 && die "Missing value for the optional argument '${_key}'." 1
                _arg_parallel="$2"
                shift
                ;;
            --prefix)
                test $# -lt 2 && die "Missing value for the optional argument '${_key}'." 1
                _arg_prefix="$2"
                shift
                ;;
            --builddir)
                test $# -lt 2 && die "Missing value for the optional argument '${_key}'." 1
                _arg_builddir="$2"
                shift
                ;;
            --destdir)
                test $# -lt 2 && die "Missing value for the optional argument '${_key}'." 1
                export DESTDIR="$2"
                shift
                ;;
            --verbose)
                _arg_verbose="on"
                ;;
            -h|--help)
                print_help
                exit 0
                ;;
            *)
                _PRINT_HELP=yes die "FATAL ERROR: Unexpected argument '$1'" 1
                ;;
        esac
        shift
    done
}

parse_commandline "$@"

if [[ "${_arg_debug_asan}" == on && "${_arg_debug_full}" == on ]]; then
  die "Only one debug option can be used." 1
fi

declare -a module_list
if module is-loaded intel || module is-loaded gcc; then
  echo "Compiler module found. Assuming that all needed modules are loaded."
  modules_present=yes
else
  # get module list
  module_script="${_script_dir}/modules_octopus_mpcdf.sh"
  module_output=$("${module_script}")
  module_list=()
  while IFS='' read -r line; do
    module_list+=("${line}")
  done <<<"${module_output}"
  # load the modules
  module load "${module_list[@]}"
  modules_present=no
fi

module load "${module_list[@]}"

if module is-loaded intel; then
  compiler=intel
elif module is-loaded gcc; then
  compiler=gcc
else
  die "No supported compiler module found. Please load gcc or intel modules." 1
fi

if module is-loaded impi; then
  mpi=impi
elif module is-loaded openmpi; then
  mpi=openmpi
else
  die "No supported mpi module found. Please load openmpi or impi modules." 1
fi

declare -a CUDA ROCM MKL
if module is-loaded cuda; then
  CUDA=("-DOCTOPUS_CUDA:BOOL=ON")
fi
if module is-loaded rocm; then
  ROCM=("-Wno-deprecated" "-DCMAKE_MODULE_PATH:STRING=${HIP_PATH}/lib/cmake/hip" "-DOCTOPUS_HIP:BOOL=ON")
fi

unset BOOST_ROOT
export CMAKE_PREFIX_PATH="${BOOST_HOME}/lib64/cmake:${CMAKE_PREFIX_PATH}"
export CMAKE_PREFIX_PATH="${CGAL_HOME}/lib64/cmake:${CMAKE_PREFIX_PATH}"
export CMAKE_PREFIX_PATH="${METIS_HOME}:${CMAKE_PREFIX_PATH}"
export CMAKE_PREFIX_PATH="${PARMETIS_HOME}:${CMAKE_PREFIX_PATH}"
export CMAKE_PREFIX_PATH="${ETSF_IO_HOME}:${CMAKE_PREFIX_PATH}"
export CMAKE_PREFIX_PATH="${HDF5_HOME}:${CMAKE_PREFIX_PATH}"
if module is-loaded pfft; then
  export CMAKE_PREFIX_PATH="${MKL_PARTS_HOME}:${CMAKE_PREFIX_PATH}"
  export CMAKE_PREFIX_PATH="${FFTW_HOME}:${CMAKE_PREFIX_PATH}"
  export CMAKE_PREFIX_PATH="${PFFT_HOME}:${CMAKE_PREFIX_PATH}"
  export PKG_CONFIG_PATH="${LIBVDWXC_HOME}/lib/pkgconfig:${PKG_CONFIG_PATH}"
  SCALAPACK_PKGCONFIG="$(mktemp -d)"
  export SCALAPACK_PKGCONFIG
  trap 'rm -rf "${SCALAPACK_PKGCONFIG}"' EXIT
  cat <<EOF > "${SCALAPACK_PKGCONFIG}/scalapack.pc"
Name: scalapack
Description: Intel(R) Math Kernel Library
Version: 1
Libs: -L${MKL_PARTS_HOME}/lib -lmkl_cluster -lm -ldl
EOF
  export PKG_CONFIG_PATH="${SCALAPACK_PKGCONFIG}:${PKG_CONFIG_PATH}"
  MKL=("-DOCTOPUS_MKL:BOOL=OFF" "-DOCTOPUS_FFTW:BOOL=ON" "-DBLAS_LIBRARIES=${MKL_PARTS_HOME}/lib/libmkl_blas.so" "-DLAPACK_LIBRARIES=${MKL_PARTS_HOME}/lib/libmkl_lapack.so" "-DCMAKE_REQUIRE_FIND_PACKAGE_ELPA:BOOL=OFF")
else
  export CMPLR_ROOT="${MKLROOT}/../../compiler/latest"
  export CMAKE_PREFIX_PATH="${MKL_HOME}:${CMAKE_PREFIX_PATH}"
  MKL=("-DOCTOPUS_MKL:BOOL=ON" "-DOCTOPUS_FFTW:BOOL=OFF")
fi

# compiler and linking flags
declare -a LINK_OPTIONS
if [[ "${compiler}" == intel ]]; then
  if [[ "${_arg_debug_asan}" == on ]]; then
    die "Debug build with AddressSanitizer not supported for Intel compiler." 1
  fi
  if [[ "${_arg_debug_full}" == on ]]; then
    die "Full debug build not supported for Intel compiler." 1
  fi
  export CFLAGS="-O3 -g -fp-model precise"
  export CXXFLAGS="${CFLAGS}"
  export FFLAGS="${CFLAGS}"
  if [[ "${mpi}" == impi ]]; then
    export CC=mpiicx
    export CXX=mpiicpx
    export FC=mpiifx
  fi
  LINK_OPTIONS+=("-DCMAKE_INSTALL_RPATH:STRING=\$ORIGIN/../lib;${GCC_HOME}/lib64 -DCMAKE_LIBRARY_PATH:STRING=${GCC_HOME}/lib64")
elif [[ "${compiler}" == gcc ]]; then
  if [[ "${_arg_debug_asan}" == on ]]; then
    # debug build with AddressSanitizer support
    export CFLAGS="-O2 -fno-unroll-loops -g3 -fno-omit-frame-pointer -fsanitize=address"
    export CXXFLAGS="${CFLAGS}"
    export FFLAGS="${CFLAGS} -fno-var-tracking-assignments"
    export LDFLAGS="-fsanitize=address"
    cat <<EOF > "${PWD}/debug_options.sh"
export ASAN_OPTIONS="symbolize=1"
export LSAN_OPTIONS="suppressions=${PWD}/lsan.supp:print_suppressions=1"
export OMPI_MCA_memory="^patcher" # https://github.com/open-mpi/ompi/issues/12819
EOF
    cat <<EOF > "${PWD}/lsan.supp"
# https://github.com/open-mpi/ompi/issues/12584
leak:libmpi.*
leak:libopen-pal.*
leak:libpmix.*
leak:hwloc_bitmap_alloc
leak:__vasprintf_internal
EOF
  elif [[ "${_arg_debug_full}" == on ]]; then
    # full debug build with bounds checking and FPE trapping
    export CFLAGS="-O0 -g3 -fno-unroll-loops -fno-omit-frame-pointer"
    export CXXFLAGS="${CFLAGS}"
    export FFLAGS="${CFLAGS} -fno-var-tracking-assignments -fcheck=bounds -ffpe-trap=invalid,zero,overflow -finit-real=snan -Wunused-variable -Wconversion"
  else
    # optimized build
    export CFLAGS="-O3 -g"
    export CXXFLAGS="${CFLAGS}"
    export FFLAGS="${CFLAGS} -fno-var-tracking-assignments"
  fi
  if [[ "${mpi}" == impi ]]; then
    export CC=mpigcc
    export CXX=mpig++
    export FC=mpigfortran
  fi
  LINK_OPTIONS+=("-DCMAKE_INSTALL_RPATH:STRING=\$ORIGIN/../lib")
fi
if [[ "${mpi}" == openmpi ]]; then
  export CC=mpicc
  export CXX=mpicxx
  export FC=mpifort
fi

# configuration step
if [[ "${_arg_config}" == on ]]; then
  cmake -S . -B "${_arg_builddir}" --fresh -G Ninja \
        -DCMAKE_BUILD_TYPE="${_build_type}" \
        -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=ON \
        "${LINK_OPTIONS[@]}" \
        -DCMAKE_INSTALL_LIBDIR:STRING=lib \
        -DCMAKE_INSTALL_PREFIX:STRING="${_arg_prefix}" \
        "${CUDA[@]}" "${ROCM[@]}" \
        "${MKL[@]}" \
        -DOCTOPUS_MPI:BOOL=ON \
        -DOCTOPUS_OpenMP:BOOL=ON \
        -DOCTOPUS_ScaLAPACK:BOOL=ON \
        -DOCTOPUS_NATIVE:BOOL=ON \
        -DOCTOPUS_UNIT_TESTS:BOOL=OFF \
        -DOCTOPUS_APP_TESTS:BOOL=ON \
        -DOCTOPUS_TESTS_REPORT:BOOL=ON \
        -DOCTOPUS_SHARED_LIBS:BOOL=ON \
        -DCMAKE_REQUIRE_FIND_PACKAGE_Libxc:BOOL=ON \
        -DCMAKE_REQUIRE_FIND_PACKAGE_Spglib:BOOL=ON

fi

# build step
cmake --build "${_arg_builddir}" ${_arg_verbose:+"--verbose"} --parallel "${_arg_parallel}"

# install step
if [[ "${_arg_install}" == on ]]; then
  cmake --build "${_arg_builddir}" ${_arg_verbose:+"--verbose"} --target install
fi

if [[ "${modules_present}" == no ]]; then
  echo
  echo "Please load the following modules in your slurm script:"
  echo "module load ${module_list[*]}"
  echo
  echo "Alternatively, you can use the following line in your slurm script:"
  # shellcheck disable=SC2312
  echo "module load \$($(readlink -f "${module_script}"))"
fi
if [[ "${_arg_debug_asan}" == on ]]; then
  echo
  echo "To use AddressSanitizer, please source the following file"
  echo "to set the environment variables needed:"
  echo "source ${PWD}/debug_options.sh"
fi
