#!/usr/bin/env bash
set -euo pipefail

_arg_toolchain=""
_arg_pfft="off"

die()
{
    _die_ret="${2:-1}"
    test "${_PRINT_HELP:-no}" = yes && print_help >&2
    echo "$1" >&2
    exit "${_die_ret}"
}

print_help()
{
    printf '%s\n' "Script for loading modules on MPCDF systems for building octopus."
    print_usage
}

print_usage()
{
    printf '\nUsage: %s [OPTIONS]\n' "$0"
    printf '\t%s\n' "--toolchain TOOLCHAIN: use TOOLCHAIN consisting of compiler and mpi (gcc_openmpi, gcc_impi, intel_impi or intel_openmpi, default: based on system detection)"
    printf '\t%s\n' "--pfft: enable FFTs based on PFFT, disables ELPA"
    printf '\t%s\n' "-h, --help: print help"
}

parse_commandline()
{
    while test $# -gt 0
    do
        _key="$1"
        case "${_key}" in
            --toolchain)
                test $# -lt 2 && die "Missing value for the optional argument '${_key}'." 1
                _arg_toolchain="$2"
                shift
                ;;
            --pfft)
                _arg_pfft="on"
                ;;
            -h|--help)
                print_help
                exit 0
                ;;
            *)
                _PRINT_HELP=yes die "FATAL ERROR: Unexpected argument '$1'" 1
                ;;
        esac
        shift
    done
}

parse_commandline "$@"

base_modules=("cmake" "ninja")
dependent_modules_default=("mkl/2025.0" "elpa/mpi/openmp" "gsl" "libxc" "spglib" "metis-64" "parmetis-64" "hdf5-serial" "netcdf-serial" "etsf_io" "cgal" "boost" "nlopt")
dependent_modules_pfft=("mkl_parts-mpi" "fftw-mpi" "pfft" "libvdwxc-mpi" "gsl" "libxc" "spglib" "metis-64" "parmetis-64" "hdf5-serial" "netcdf-serial" "etsf_io" "cgal" "boost" "nlopt")
if [[ "${_arg_pfft}" == "off" ]]; then
  dependent_modules=("${dependent_modules_default[@]}")
elif [[ "${_arg_pfft}" == "on" ]]; then
  dependent_modules=("${dependent_modules_pfft[@]}")
else
  die "Internal error." 1
fi
module_list_gcc_openmpi=("${base_modules[@]}" "gcc/13" "openmpi/5.0" "${dependent_modules[@]}")
module_list_gcc_impi=("${base_modules[@]}" "gcc/13" "impi/2021.14" "${dependent_modules[@]}")
module_list_intel_impi=("${base_modules[@]}" "intel/2025.0" "impi/2021.14" "${dependent_modules[@]}" "gcc/13")
module_list_intel_openmpi=("${base_modules[@]}" "intel/2025.0" "openmpi/5.0" "${dependent_modules[@]}" "gcc/13")
module_list_gcc_cuda=("${base_modules[@]}" "gcc/13" "cuda/12.6" "openmpi_gpu/5.0" "${dependent_modules[@]}")
module_list_gcc_rocm=("${base_modules[@]}" "gcc/13" "rocm/6.3" "openmpi_gpu/5.0" "${dependent_modules[@]}")

case "${_arg_toolchain}" in
  "")
    # not specified, so use toolchains based on system
    # shellcheck disable=SC2154
    case ${SLURM_CELL} in
      ADA|RAVEN)
        module_list=("${module_list_gcc_cuda[@]}")
        ;;
      VIPER)
        hostname="$(hostname)"
        if [[ "${hostname}" == "viper12i" ]]; then
          # we are on VIPER-GPU
          module_list=("${module_list_gcc_rocm[@]}")
        else
          # we are on VIPER-CPU
          module_list=("${module_list_gcc_openmpi[@]}")
        fi
        ;;
      *)
        echo "Could not detect MPCDF system, aborting."
        exit 1
    esac
    ;;
  gcc_openmpi)
    module_list=("${module_list_gcc_openmpi[@]}")
    ;;
  gcc_impi)
    module_list=("${module_list_gcc_impi[@]}")
    ;;
  intel_impi)
    module_list=("${module_list_intel_impi[@]}")
    ;;
  intel_openmpi)
    module_list=("${module_list_intel_openmpi[@]}")
    ;;
  *)
    die "Value ${_arg_toolchain} for toolchain not known." 1
    ;;
esac

printf "%s\n" "${module_list[@]}"
