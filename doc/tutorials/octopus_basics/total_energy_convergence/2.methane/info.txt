Eigenvalues [eV]
 #st  Spin   Eigenvalue      Occupation
   1   --   -15.990965       2.000000
   2   --    -9.065615       2.000000
   3   --    -9.065615       2.000000
   4   --    -9.065615       2.000000

Energy [eV]:
      Total       =      -219.03734302
      Free        =      -219.03734302
      -----------
      Ion-ion     =       236.08498119
      Eigenvalues =       -86.37561817
      Hartree     =       393.45794769
      Int[n*v_xc] =      -105.38062665
      Exchange    =       -69.66881505
      Correlation =       -11.00056995
      vanderWaals =         0.00000000
      Delta XC    =         0.00000000
      Entropy     =         0.00000000
      -TS         =        -0.00000000
      Photon ex.  =         0.00000000
      Kinetic     =       163.97228711
      External    =      -931.88342144
      Non-local   =       -49.75772366
      Int[n*v_E]  =         0.00000000

