#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt *.eps

./ch.sh

gnuplot plot.gp


cp tutorial.sh ch.sh plot.gp *.txt *.eps ch.dat $OCTOPUS_TOP/doc/tutorials/other/geometry_optimization/1.methane/1.script/
