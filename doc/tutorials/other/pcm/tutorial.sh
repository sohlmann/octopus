#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

cp inp_gs inp

export OMP_NUM_THREADS=2
mpirun -n 4 octopus |tee log

$HELPER_DIR/extract_generic.sh static/info  "Energy" "Dipole" | head -n -1 > energy.txt

head -n 5 pcm/cavity_mol.xyz > cavity_mol.xyz
head -n 22 pcm/pcm_info.out > pcm_info.txt

cp inp_td inp

mpirun -n 4 octopus |tee log


cp inp_gs inp_td tutorial.sh *.txt cavity_mol.xyz  vib_modes/normal_frequencies_lr $OCTOPUS_TOP/doc/tutorials/other/pcm
