#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt *.eps

./run.sh

gnuplot plot.gp


cp tutorial.sh inp plot.gp run.sh *.dat *.eps  $OCTOPUS_TOP/doc/tutorials/other/rdmft/3.convergence
