set xlabel "Energy (1/cm)"
set ylabel "Spectrum (a.u.)"
set t postscript enhanced color font "Monospace-Bold,25" landscape size 11,8.5
set output "BOMD_H2O.eps"

unset key

set rmargin 4.5
set lmargin 10.5
set tmargin 3.2
set bmargin 5.5


plot "td.general/vibrational_spectrum" u 1:2 w l lw 3
