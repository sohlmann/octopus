#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt *.eps
oct-vibrational_spectrum |tee log

gnuplot plot.gp

cp tutorial.sh *.txt *.eps $OCTOPUS_TOP/doc/tutorials/other/bomd/3-results/
