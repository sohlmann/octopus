#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt
rm -rf restart/

octopus >& log


cp tutorial.sh *.txt $OCTOPUS_TOP/doc/tutorials/other/arpes/01-gs/
