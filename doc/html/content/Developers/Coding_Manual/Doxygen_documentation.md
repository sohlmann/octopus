---
title: "Doxygen documentation"
section: "Developers"
weight: 10
authors: "Martin Lueders, Alex Buccheri"
---

### Online doxygen documentation

The {{<command "Doxygen">}} documentation for the {{<code "main">}} branch of Octopus can be found [here](https://www.octopus-code.org/doc/main/doxygen_doc/). 
It is automatically generated at a push to the main branch.

{{<notice "note">}}
Currently, the links to the source code are broken and will most likely point to a wrong part of the file.
{{</notice>}}

### Running doxygen locally

To build the {{<command "Doxygen">}} pages locally, go into the folder {{<file "doc/doxygen/">}} inside the {{<octopus>}} tree, and run the command 
```shell
./create_documentation.sh
```
 This might take a few minutes to complete. In case you need to rebuild the pages more often while writing new documentation, you might want to disable the creation of call graphs, by hanging the line
```
HAVE_DOT = YES
```
to 
```
HAVE_DOT = NO
```
in the file {{<file "Doxyfile">}}. 


### Writing documentation

There are several ways how to include comments into the Fortran source, which are shown in the following code snippet:

In particular, we would like to have documentation of modules, classes, and subroutines/functions, together with their data members and arguments.

#### General remarks about Doxygen

{{<command "Doxygen">}} markers in Fortran are `!>`, `!<` and `!!`. The first two denote the beginning of a {{<command "Doxygen">}} comment,
and the direction of the ‘arrow’ indicates what the comment refers to. In general, if a comment is before the
quantity which should be documented, one should use `!>`, while if the comment is at the end of the line
(e.g. for an function argument), one should use `!<`. Continuation lines use the `!!` marker.
{{% note info %}}
Using the wrong marker can lead to wrong and confusing documentation.
{{% /note %}}

A continuation of a comment to a function argument, written as
```Fortran
integer, intent(in) :: dim !< Dimension of space
                           !! needs to be in the range 1 to 4.
```
is not accepted by {{<command "findent-octopus">}} and will lead to a failing test.

Instead, this should be written as
```Fortran
integer, intent(in) :: dim !< Dimension of space
!!                            needs to be in the range 1 to 4.
```

#### Markdown and Equations in Doxygen

{{<command "Doxygen">}} supports Markdown syntax for formatting text. More details can be found here.

Furthermore, we enabled MathJax, which allows to use LaTeX formatted equations in the code documentation. 
Mathematical formulas have to be enclosed by a pair of `\f$` for inline formulas, or by the pair of `\f[` and `\f]` 
for formulas to be displayed as separate line. See https://www.doxygen.nl/manual/formulas.html.
Module documentation

Before the declaration of the module, there should be a block
```Fortran
!> @brief This modules implements ...
!!
!! Some more details about the modules, how it is related to the code, general theory, 
!! references to papers, etc.
!!
module module1_oct_m
...
end module 
```

There is one peculiarity about the Doxygen usage in Octopus, which is that we need to pass the sources through
the cpp preprocessor in order to handle some macros, in particular out poor-man's templating. This has the 
side effect that the preprocessor removes all C/C++ like comments from the code, including the '//' in URLs.

URLs can be included in Doxygen comments by using the HTTP and HTTPS macros.
```Fortran
!> @brief The Octopus code
!!
!! The web documentation can be accessed \HTTPS{here,octopus-code.org}.
!!
```

#### Class documentation

Before the declaration of the module, there should be a block
```Fortran
!> @brief This class implements ..
!!
!! Some more details about the class, which is not in the module description
!! or in the documentation of member data or procedure
!!
type class1_t
private

class(system_t)    :: system         !< This system is related to the current object
logical, public    :: is_initialized !< indicates whether all initialization has been successfully done
...
contains
    ! Below are the methods, the class implements
    procedure :: do_something => class1_do_something    !< @copydoc module1_oct_m::class1_do_something
    procedure :: finished => class1_finished            !< @copydoc module1_oct_m::class1_finished
    ...
end type
```

The [`@copydoc`](https://www.doxygen.nl/manual/commands.html#cmdcopydoc) command here instructs 
{{<command "Doxygen">}} to copy the documentation from the actual function definition for the class method. 

Note that the corresponding module name has to be prepended (in C++ codes, this would be the namespace).

#### Subroutine / Function documentation

The documentation of functions and subroutines is similar to that of classes. 

#### Example

{{% expand "Example" %}}
```Fortran
#include_file src/multisystem/propagators/propagator.F90
```
{{% /expand %}}

This leads to the following [Doxygen output](https://octopus-code.org/doc/main/doxygen_doc/propagator_8F90.html)
