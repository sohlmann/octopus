---
title: "Installing Octopus with Autotools"
#series: "Manual"
weight: 2
---



## Downloading

Download the latest {{< octopus >}} version here: {{< octopus-download >}}.


If you have a different system from those mentioned above or you want to compile {{< octopus >}} you need to get the source code file ({{< file ".tar.gz" >}} file) and follow the compilation instructions below.

{{%notice note%}}
Make sure you download the correct tar-ball, depending on whether you want to use CMake (recommended) or Autotools
{{%/notice%}}

## Building with Autotools

This page assumes, that the required are already installed on your system, either as system libraries, or as available modules.
In case some library is missing, you can find information on {{<manual "installation/building_from_scratch" "this page">}}.


### Quick instructions

For the impatient, here is the quick-start:


```bash
$ tar xzf octopus-{{< octopus-version >}}.tar.gz
$ cd octopus-{{< octopus-version >}}
$ ./configure --prefix={{<command "<INSTALLATION_DIR>">}}
$ make
$ make install
```

This will probably {{< emph "not" >}} work, so before giving up, just read
the following paragraphs.


### Long instructions

The code is written in standard {{< name "Fortran 2008" >}}, with some routines written in {{< name "C" >}} (and in {{< name "bison" >}}, if we count the input parser). To build it you will need both a {{< name "C" >}} compiler ({{< name "gcc" >}} works just fine and it is available for almost every piece of silicon), and a
{{< name "Fortran 2008" >}} compiler. 


#### Requirements

Besides the compiler, you will also need:

* {{< name "make" >}}: most computers have it installed, otherwise just grab and install the {{< name "GNU make" >}}.

* {{< name "cpp" >}}: The {{< name "C" >}} preprocessor is heavily used in {{< octopus >}} to preprocess {{< name "Fortran" >}} code. It is used for both C (from the CPP variable) and Fortran (FCCPP). {{< name "GNU cpp" >}} is the most convenient but others may work too. For more info, see {{< versioned-link "Developers/Coding_Manual/Preprocessors" "Preprocessors" >}}.

* {{< name "Libxc" >}}: The library of exchange and correlation functionals. It used to be a part of {{< octopus >}}, but since version 4.0.0 it is a standalone library and needs to be installed independently. For more information, see the [libxc page](https://libxc.gitlab.io/). Please, check the {{<versioned-link "Releases" "releases pages">}} for information on the required libxc version.


* {{< name "FFTW" >}}: We have relied on this great library to perform Fast Fourier Transforms ({{< name "FFTs" >}}). You may grab it from the [{{< name "FFTW" >}} site](https://www.fftw.org/). You require {{< name "FFTW" >}} version 3.

* {{< name "LAPACK/BLAS" >}}: Our policy is to rely on these two libraries as much as possible on these libraries for linear-algebra operations. If you are running {{< name "Linux" >}}, there is a fair chance they are already installed in your system. The same goes to the more heavy-weight machines ({{< name "alphas" >}}, {{< name "IBMs" >}}, {{< name "SGIs" >}}, etc.). Otherwise, just grab the source from [{{< name "netlib" >}} site](https://www.netlib.org).

* {{< name "GSL" >}}: Finally someone had the nice idea of making a public scientific library! {{< name "GSL" >}} still needs to grow, but it is already quite useful and impressive. {{< octopus >}} uses splines, complex numbers, special functions, etc. from {{< name "GSL" >}}, so it is a must! If you don't have it already installed in your system, you can obtain {{< name "GSL" >}} from the [{{< name "GSL" >}} site](https://www.gnu.org/software/gsl/). You will need version 1.9 or higher. 

* {{< name "Perl" >}}: During the build process {{< octopus >}} runs several scripts in this language. It's normally available in every modern {{< name "Unix" >}} system.


#### Optional libraries


There are also some optional packages; without them some parts of {{< octopus >}} won't work:

* {{< name "MPI" >}}: If you want to run {{< octopus >}} in multi-tentacle (parallel) mode, you will need an implementation of {{< name "MPI" >}}. [MPICH](https://www-unix.mcs.anl.gov/mpi/mpich/) or [Open MPI](https://www.open-mpi.org/) work just fine in our {{< name "Linux" >}} boxes.

* {{< name "PFFT" >}}: We rely on this great library for highly scalable parallel Poisson solver, based on Fast Fourier Transforms ({{< name "FFTs" >}}). You may grab it from the [{{< name "M. Pippig's" >}} site](https://www-user.tu-chemnitz.de/~potts/workgroup/pippig/software.php.en). You also require {{< name "FFTW" >}} version 3.3 compiled with MPI and with a small patch by M. Pippig (also available there).

* {{< name "NetCDF" >}}: The [Network Common Dataform](https://www.unidata.ucar.edu/software/netcdf/) library is needed for writing the binary files in a machine-independent, well-defined format, which can also be read by {{< manual "Visualization" "visualization programs" >}} such as [OpenDX](https://www.opendx.org/)

* {{< name "GDLib" >}}: A library to read graphic files. See {{< tutorial "Model/Particle in an octopus" "Particle in an octopus" >}}. (The simulation box in 2D can be specified via {{< variable "BoxShapeImage" >}}.) Available from [GDLib](https://www.libgd.org).

* {{< name "SPARSKIT" >}}: [Library for sparse matrix calculations](https://www-users.cs.umn.edu/~saad/software/SPARSKIT/). Used for one propagator technique.

* {{< name "ETSF I/O" >}}: An input/output library implementing the ETSF standardized formats, requiring NetCDF, available at [libraries_and_tools](https://github.com/ElectronicStructureLibrary/libetsf_io/). Versions 1.0.2, 1.0.3, and 1.0.4 are compatible with {{< octopus >}} (though 1.0.2 will produce a small discrepancy in a filesize in the testsuite). It must have been compiled with the same compiler you are using with {{< octopus >}}. To use ETSF_IO, include this in the <tt>configure</tt> line, where <tt>$DIR</tt> is the path where the library was installed:

{{< code-line "--with-etsf-io-prefix=\"$DIR\"">}}


* {{< name "PSOLVER" >}}: [PSOLVER](https://gitlab.com/l_sim/psolver) (version 10.0 and above) is a highly scalable parallel Poisson solver, with a cheap memory footprint. The corresponding configure line is:

{{< code-block >}}
  --with-psolver-prefix=DIR
                          Directory where PSolver was installed.
  --with-psolver-include=DIR
                          Directory where PSolver Fortran headers were
                          installed.
{{< /code-block >}}

#### Unpacking the sources

Uncompress and untar it ({{< command "gzip -cd octopus-{{< octopus_version >" >}}.tar.gz | tar -xvf -}}). In the following, {{< file "OCTOPUS-SRC/" >}} denotes the source directory of {{< octopus >}}, created by the {{< command "tar" >}} command.


The {{< file "OCTOPUS-SRC/" >}} contains the following subdirectories of interest to users:

* {{< file "doc/" >}}: The documentation of {{< octopus >}}, mainly in HTML format.

* {{< file "liboct_parser/" >}}: The C library that handles the input parsing.

* {{< file "share/PP/" >}}: Pseudopotentials. In practice now it contains the Troullier-Martins (PSF and UPF formats) and Hartwigsen-Goedecker-Hutter pseudopotential files.

* {{< file "share/util/" >}}: Currently, the {{< emph "utilities" >}} include a couple of IBM OpenDX networks ({{< file "mf.net" >}}), to visualize wavefunctions, densities, etc.

* {{< file "testsuite/" >}}: Used to check your build. You may also use the files in here as samples of how to do various types of calculations.

* {{< file "src/" >}}: Fortran90 and C source files. Note that the Fortran90 files have to be preprocessed before being fed to the Fortran compiler, so do not be scared by all the - directives.

## Autotools

#### Development version

You can get the development version of {{< octopus >}} by downloading it from the {{< octopus >}} project on {{< octopus-git >}}.

You can also get the current version with the following command (you need the {{< name "git" >}} package):

{{< command-line "git clone git@gitlab.com:octopus-code/octopus.git" >}}

Before running the configure script, you will need to run the GNU autotools. This may be done by executing:

{{< command-line "autoreconf -i" >}}

Note that you need to have working recent versions of the {{< name "automake" >}} and {{< name "autoconf" >}}. In particular, the configure script may fail in the part <tt>checking for Fortran libraries of mpif90</tt> for <tt>autoconf</tt> version 2.59 or earlier. The solution is to update <tt>autoconf</tt> to 2.60 or later, or manually set <tt>FCLIBS</tt> in the <tt>configure</tt> command line to remove a spurious apostrophe.

If autoreconf is failing with "aclocal: warning: couldn't open directory 'm4': No such file or directory", create an empty folder named m4 inside external_libs/spglib-2.1.0/ now.

Please be aware that the development version may contain untested changes that can affect the execution and the results of {{< octopus >}}, especially if you are using new and previously unreleased features. So if you want to use the development version for production runs, you should at least contact {{< octopus >}} developers.

#### Configuring

Before configuring you can (should) set up a couple of options. Although
the {{< name "configure" >}} script tries to guess your system settings for you, we recommend
that you set explicitly the default Fortran compiler and the compiler options.
Note that {{< name "configure" >}} is a standard tool for Unix-style programs and you can find a lot of generic documentation on how it works elsewhere.

For example, in {{< name "bash" >}} you would typically do:

{{< command-line "export FC=ifort" >}}
{{< command-line "export FCFLAGS=\"-O2 -xHost\"" >}}

if you are using the Intel Fortran compiler on a linux machine.

Also, if you have some of the required libraries in some unusual directories,
these directories may be placed in the variable {{< code "LDFLAGS" >}} (e.g.,
{{< command "export LDFLAGS=$LDFLAGS:/opt/lib/" >}}).

The configuration script will try to find out which compiler you are using.
Unfortunately, and due to the nature of the primitive language that {{< octopus >}}
is programmed in, the automatic test fails very often. 

One can also manually set environment variables like {{< code "LIBS_PSOLVER" >}} and {{< code "FCFLAGS_ELPA" >}} to influence the library detection. For eg: 
{{< command-line "export LIBS_PSOLVER='-L$BIGDFT_PSOLVER_ROOT/lib -lPSolver-1 -lgomp'" >}}
{{< command-line "export LIBS_ELPA='-lelpa_openmp' # or -lelpa" >}}
{{< command-line "export FCFLAGS_ELPA='-I$ELPA_ROOT/include/elpa_openmp-2021.11.001/modules'" >}}
Do note however, not all dependency tests might support such environmental variables.

You can now run the configure script
{{< command-line "./configure" >}}

You can use a fair amount of options to spice {{< octopus >}} to your own taste. To obtain a full list just type {{< command "./configure --help" >}}. Some
commonly used options include:

* {{< flag "--prefix=" >}}{{< inst_file >}}: Change the base installation dir of {{< octopus >}} to {{< inst_file >}}. {{< inst_file >}} defaults to the home directory of the user who runs the {{< name "configure" >}} script.

* {{< flag "--with-fft-lib=" >}}{{< file "<lib>" >}}: Instruct the {{< name "configure" >}} script to look for the {{< name "FFTW" >}} library exactly in the way that it is specified in the {{< file "<lib>" >}} argument. You can also use the {{< code "FFT_LIBS" >}} environment variable.

* {{< flag "--with-pfft-prefix=" >}}{{< file "DIR/" >}}: Installation directory of the {{< name "PFFT" >}} library.

* {{< flag "--with-pfft-lib=" >}}{{< file "<lib>" >}}: Instruct the {{< name "configure" >}} script to look for the {{< name "PFFT" >}} library exactly in the way that it is specified in the {{< file "<lib>" >}} argument. You can also use the {{< code "PFFT_LIBS" >}} environment variable.

* {{< flag "--with-blas=" >}}{{< file "<lib>" >}}: Instruct the {{< name "configure" >}} script to look for the {{< name "BLAS" >}} library in the way that it is specified in the {{< file "<lib>" >}} argument.

* {{< flag "--with-lapack=" >}}{{< file "<lib>" >}}: Instruct the {{< name "configure" >}} script to look for the {{< name "LAPACK" >}} library in the way that it is specified in the {{< file "<lib>" >}} argument.

* {{< flag "--with-gsl-prefix=" >}}{{< file "DIR/" >}}: Installation directory of the {{< name "GSL" >}} library. The libraries are expected to be in {{< file "DIR/lib/" >}} and the include files in {{< file "DIR/include/" >}}. The value of {{< file "DIR/" >}} is usually found by issuing the command {{< command "gsl-config --prefix" >}}.

* {{< flag "--with-libxc-prefix=" >}}{{< file "DIR/" >}}: Installation directory of the {{< name "Libxc" >}} library.

If you have problems when the {{< name "configure" >}} script runs, you can find more details of what happened in the file {{< name "config.log" >}} in the same directory.

#### Compiling and installing

Run {{< command "make" >}} and then {{< command "make install" >}}. The compilation may take some time, so you might want to speed it up by running {{< command "make" >}} in parallel ({{< command "make -j" >}}). If everything went fine, you should now be able to taste {{< octopus >}}.

Depending on the value given to the {{< flag "--prefix" >}}={{< inst_file >}} given, the executables will reside in {{< inst_file "bin/" >}}, and the auxiliary files will be copied to {{< inst_file "share/octopus" >}}.

#### Testing your build

After you have successfully built {{< octopus >}}, to check that your build works as expected there is a battery of tests that you can run. They will check that {{< octopus >}} executes correctly and gives the expected results (at least for these test cases). If the parallel version was built, the tests will use up to 6 MPI processes, though it should be fine to run on only 4 cores. (MPI implementations generally permit using more tasks than actual cores, and running tests this way makes it likely for developers to find race conditions.)

To run the tests, in the sources directory of {{< octopus >}} use the command

{{< command-line "make check" >}}

or if you are impatient,

{{< command-line "make check-short" >}}

which will start running the tests, informing you whether the tests are passed or not.

If all tests fail, maybe there is a problem with your executable (like a missing shared library).

If only some of the tests fail, it might be a problem when calling some external libraries (typically blas/lapack). Normally it is necessary to compile all Fortran libraries with the same compiler. If you have trouble, try to look for help in the [{{< octopus >}} mailing list](https://listserv.gwdg.de/pipermail/octopus-users/).

#### Fast recompilation

If you have already compiled the code and if you are changing only one file, you can run

{{< command-line "make NODEP=1" >}}

to ignore the dependencies due to Fortran module files. This will only compile the files that have changed and link the executables; therefore, it is much faster. If you change, e.g., interfaces of modules or functions, you need to to run {{< command "make" >}} without {{< command "NODEP=1" >}} to ensure a correct handling of the dependencies.


---------------------------------------------
