---
title: "Vibrational modes"
#tags: ["Tutorial", "Advanced", "Vibrational Modes", "Geometry Optimization", "Molecule", "Pseudopotentials", "DFT", "Forces", "Vibrations"]
series: "Tutorials"
theories: "DFT"
calculation_modes: ["Geometry Optimization"]
system_types: "Molecule"
species_types: "Pseudopotentials"
features: ["Vibrational Modes","Forces"]
description: "Obtain vibrational modes by using Sternheimer linear response."
---


This tutorial will show you how to obtain vibrational modes in {{< octopus >}} by using Sternheimer linear response. We will use the water molecule as example. To start we will need the geometry. Copy the following in a file called {{< file "h2o.xyz" >}}:

{{< code-block >}}
#include_file doc/tutorials/other/vibrational_modes/h2o.xyz
{{< /code-block >}}

Since to obtain proper vibrational modes we need to be as close as possible to the minimum of energy, first we will optimize the geometry under the parameters we have defined for the simulation. For that we use the following input file:

{{< code-block >}}
#include_input doc/tutorials/other/vibrational_modes/inp_go
{{</code-block>}}

As you see we will use a smaller than usual spacing and a larger spherical box. This is required since forces require more precision than other quantities. Of course for production runs these values should be properly converged. Also to increase the precision of the forces and the total energy we will use the variable {{< variable "FilterPotentials" >}}, which specifies that a filter should be applied to the ionic potentials that removes the Fourier components that are higher than what we can represent on our grid.

Now run {{< octopus >}}, once the calculation is finished you should get the optimized geometry in {{< file "min.xyz" >}}. Modify the value of {{< variable "XYZCoordinates" >}} to point to this file.

To perform a calculation of the vibrational modes we need a ground-state calculation with a very well converged forces. So change the {{< variable "CalculationMode" >}} to {{< code "gs" >}} and run {{< octopus >}} again. Once done, check that the forces in the {{< file "static/info" >}} file are reasonably small (<0.05 eV/A).

Now we are ready for the actual calculation of vibrational modes. To do it, change {{< variable "CalculationMode" >}} to {{< code "vib_modes" >}} and run {{< octopus >}}. This calculation will take a while, since $3N_{atoms}$ response calculations are required.

Finally, the results will be written into the {{< code "vib_modes" >}} folder.

- Check the {{< file "normal_frequencies_lr" >}} file.

{{< code-block >}}
#include_file doc/tutorials/other/vibrational_modes/normal_frequencies_lr
{{< /code-block >}}

What kind of information does it give? Did we find the correct equilibrium geometry?

Try now to visualize the vibrational eigenmodes. You can use XCrySDen to open the file {{< file "normal_modes_lr.axsf" >}}. In the "Display" menu set "Forces", and in "Modify" set the factor to 1. Try to classify the modes as translations, rotations, and true internal vibrations. Which ones have the "negative" (actually imaginary) frequencies?
